  require 'sinatra'
  require 'active_record'
  require 'json'
  require 'net/http'
  require 'savon'
  require 'faraday'

  
  STRRESPONSE = "RESPONSE"
  STRFALSE = "FALSE"
  STRTRUE = "TRUE"
  SEQUENCE = "SEQUENCE"
  STRMRCHNT = "233261064828"
  STRNCKNAME = "APPSNMOBIL"



 ActiveRecord::Base.establish_connection(
    :adapter  => "mysql2",
    :host     => "localhost",
    :username => "jude",
    :password => "$jude1",
    :database => "jd_db2"
  )



  class CustomerNumber < ActiveRecord::Base
    self.table_name = 'customer_numbers'
  end

  class SalesStaffOutlet < ActiveRecord::Base
   self.table_name = 'sales_staff_outlets'
  end

  class Transaction < ActiveRecord::Base
   self.table_name = 'transactions'
  end


  class USSDRequest < ActiveRecord::Base
   self.table_name = 'ussd_requests'
  end
  
  
  class SalesStaff < ActiveRecord::Base
    self.table_name = 'sales_staffs'
  end


  post '/' do
       request.body.rewind
       @payload=JSON.parse(request.body.read)
       json_vals = @payload
       sequence=json_vals['SEQUENCE']
       end_session=json_vals['END_OF_SESSION']
       sessionID=json_vals['SESSION_ID']
       service_key=json_vals['SERVICE_KEY']
       mobile_number=json_vals['MOBILE_NUMBER']
       ussd_body=json_vals['USSD_BODY'].strip if json_vals.has_key?('USSD_BODY')
       saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
       process(sequence,service_key,sessionID,mobile_number,ussd_body,end_session)
  end



  def process(sequence, service_key, sessionID, mobile, ussd_body, end_of_session='True')
    if sequence=="0"
        s_params = Hash.new
        s_params['SEQUENCE']=sequence
        s_params['REQUEST_TYPE']=STRRESPONSE
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY']= "WAEC 1. Make Payment \n 2. Statement \n 3. My Profile"
        p s_params.to_json
    elsif sequence == "1"
        s_params = Hash.new
        s_params['SEQUENCE'] = sequence
        s_params['REQUEST_TYPE'] = STRRESPONSE
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY'] = ussd_body.strip
        if ussd_body == "1"
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY']="\nPlease enter customer number:"
        #saveCustomerNumber
        elsif ussd_body == "2"
        s_params['END_OF_SESSION']=STRTRUE
        s_params['USSD_BODY']="Service is currently unavailable."
        elsif ussd_body == "3"
         @connection = ActiveRecord::Base.establish_connection(
            :adapter => "mysql2",
            :host => "localhost",
            :database => "jd_db2",
            :username => "jude",
            :password => "$jude1"
            )
      
          sql =   'select t.assigned_staff_id, t.last_name, t.other_names, t.service_phone, k.outlet_name, r.long_name from 
          jd_db2.sales_staffs t 
          left join jd_db2.sales_staff_outlets s on s.sales_staff_id = t.id 
          left join jd_db2.sales_outlets k on s.outlet_id=k.id left join jd_db2.company_masters r on t.company_id = r.id where t.service_phone ='+ mobile
         
          @result = @connection.connection.execute(sql);
          @result.each do |record|
            #Resultant Array = ["CT0010121202", "Botwe", "Kojo", "233266122452", "Osu Goil", "Seaweld Eng."]
            @vendingStaffName = record[2] + " " + record[1]
            @staffID = record[0]
            @outletName = record[4] 
            @company_name = record[5]
            @service_number = record[3]
          end
          s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="My Profile \n\nSales Person: #{@vendingStaffName} \nAssigned ID: #{@staffID} \n Company: #{@company_name} \n Outlet Name: #{@outletName} \n Service Number: #{@service_number}"
      end
          p s_params.to_json
    elsif sequence == "2"
          s_params = Hash.new
          s_params['SEQUENCE'] = sequence
          s_params['REQUEST_TYPE'] = STRRESPONSE
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY'] = ussd_body.strip
          
          if ussd_body.scan(/\D/i).length == 0
            s_params['END_OF_SESSION']=STRFALSE
            s_params['USSD_BODY']="\n Please enter amount:"
          else
            s_params['END_OF_SESSION']=STRTRUE
            s_params['USSD_BODY']="\n Invalid number.Please try again later."
          end
          p     s_params.to_json
    elsif sequence == "3"
          s_params = Hash.new
          s_params['SEQUENCE'] = sequence
          s_params['REQUEST_TYPE'] = STRRESPONSE
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY'] = ussd_body.strip

          if ussd_body.scan(/\D/i).length == 0
            s_params['END_OF_SESSION']=STRTRUE
            s_params['USSD_BODY']="\n Transaction is being processed."
            #saveTransactions(sales_staff_id,customer_number,outlet_id,amount,company_id,user_id)
            customerNumber = USSDRequest.where("session_id=? and sequence=? and mobile_number=?", sessionID, "2",mobile).pluck(:ussd_body)
             puts "**This is the customer number: #{customerNumber}***"
            @customer_number = customerNumber[0]
            @amount = ussd_body.strip
            
            task = Thread.new{
                      msgHash={ merchant_number: STRMRCHNT, mobile_number: @customer_number, amount: @amount, reference: 'EasyPay', nickname: STRNCKNAME }
                      response=mobilePayment(msgHash)
                      puts response.inspect
                      
                      detailArray = getDetails(mobile)
                      user_id = detailArray[0]
                      company_id = detailArray[1]
                      staff_id = detailArray[2]
                      outlet_id = detailArray[3]
                      
                      saveTransactions(staff_id,@customer_number,outlet_id,@amount,company_id,user_id)
            }
            
          else
            s_params['END_OF_SESSION']=STRTRUE
            s_params['USSD_BODY']="\n .There was a problem please try again later."
          end
          p     s_params.to_json
           p s_params.to_json
    elsif sequence == "4"
          s_params=Hash.new
          s_params['SEQUENCE']=sequence
          s_params['REQUEST_TYPE']=STRRESPONSE
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY'] = ussd_body.strip
       if ussd_body == "1"
              s_params['END_OF_SESSION']=STRTRUE
              s_params['USSD_BODY']="Your transaction will be processed."
              p s_params.to_json
        else
         s_params['END_OF_SESSION']=STRTRUE
         s_params['USSD_BODY']="Your transaction has been cancelled."
        end
        p s_params.to_json
    end
  end


  def saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
     ussdObject = UssdRequest.new
     ussdObject.sequence = sequence
     ussdObject.service_key = service_key
     ussdObject.session_id = sessionID
     ussdObject.mobile_number = mobile_number
     ussdObject.ussd_body = ussd_body
     ussdObject.created_at = Time.now
     ussdObject.save
  end
  
 def mobilePayment(msgHash)
   client = Savon.client(wsdl: 'http://41.190.91.198:10088/wservices.php?wsdl')
   response = client.call(:purchase_info, message: msgHash)
   resp=""
   resp=response.body[:purchase_info_response][:response]
   respArr=resp.split("~")
 end
  
  
  def saveTransactions(sales_staff_id,customer_number,outlet_id,amount,company_id,user_id)
   #  sales_staff_id | outlet_id | company_id | user_id 
    transaction = Transaction.new
    transaction.sales_staff_id = sales_staff_id
    transaction.customer_number = customer_number
    transaction.outlet_id = outlet_id
    transaction.amount = amount
    transaction.company_id = company_id
    transaction.user_id = user_id
    transaction.created_at = Time.now
    transaction.save
  end
  
  
  def saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
    request = USSDRequest.new
    request.sequence = sequence
    request.service_key = service_key
    request.session_id = sessionID
    request.mobile_number = mobile_number
    request.ussd_body = ussd_body
    request.created_at = Time.now
    request.updated_at = Time.now
    request.save
  end
  
  
  def getDetails(service_number)
     id = SalesStaff.where(service_phone: service_number).select(:id)[0][:id]
     user_id = SalesStaff.where(service_phone: service_number).select(:user_id)[0][:user_id]
     company_id = SalesStaff.where(service_phone: service_number).select(:company_id)[0][:company_id]
     assigned_staff_id = SalesStaff.where(service_phone: service_number).select(:assigned_staff_id)[0][:assigned_staff_id]
     outlet_id = SalesStaffOutlet.where(sales_staff_id: id).select(:outlet_id)[0][:outlet_id]
     return user_id,company_id,assigned_staff_id,outlet_id
  end
  
  
  
  
