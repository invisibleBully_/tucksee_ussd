  require 'sinatra'
  require 'active_record'
  require 'json'
  require 'net/http'
  require 'savon'
  #require './module/modules'
  
  
  
  
  STRRESPONSE = "RESPONSE"
  STRFALSE = "FALSE"
  STRTRUE = "TRUE"
  SEQUENCE = "SEQUENCE"
  STRMRCHNT = "233261061441"
  STRNICKNME = "HFC"
  STRWRONGOPTION = "Wrong option selected. Please try again"
  STRWDR = "WDR"
  STRDEP = "DPS"
  STRBAL = "BLC"

  ActiveRecord::Base.establish_connection(
    :adapter  => "mysql2",
    :host     => "localhost",
    :username => "jude",
    :password => "$jude1",
    :database => "jd_db"
  )


  class TermsConditions < ActiveRecord::Base
    self.table_name = 'terms_conditions'
  end

  class UssdRequest < ActiveRecord::Base
   self.table_name = 'hfc_ussd_requests'
  end


  class Registration < ActiveRecord::Base
  self.table_name = 'hfc_registrations'
  self.primary_key = 'mobile_number'
  has_many :registered_users
  end

  class RegisteredUser < ActiveRecord::Base
  self.table_name= 'hfc_registered_users'
  belongs_to :registration
  has_many :transactions
  end

  class Transaction < ActiveRecord::Base
  self.table_name= 'transactions'
  belongs_to :registered_user
  end


  class TransactionResponse < ActiveRecord::Base
  self.table_name= 'transaction_responses'
  end

  post '/' do
       request.body.rewind
       @payload=JSON.parse(request.body.read)
       json_vals = @payload
       sequence=json_vals['SEQUENCE']
       end_session=json_vals['END_OF_SESSION']
       sessionID=json_vals['SESSION_ID']
       service_key=json_vals['SERVICE_KEY']
       mobile_number=json_vals['MOBILE_NUMBER']
       ussd_body=json_vals['USSD_BODY'].strip if json_vals.has_key?('USSD_BODY')
       saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
       process(sequence,service_key,sessionID,mobile_number,ussd_body,end_session)
  end



  def process(sequence, service_key, sessionID, mobile, ussd_body, end_of_session='True')
    if sequence=="0"
     s_params=Hash.new
     s_params['SEQUENCE']=sequence
       s_params['REQUEST_TYPE']=STRRESPONSE
       s_params['END_OF_SESSION']=STRFALSE
       s_params['USSD_BODY']="HFC Air Wallet Main Menu\n\n 1. Register \n 2. Speak To A Representative \n 3. Check Balance\n 4. Funds Deposit\n 5. Withdrawal"
       p s_params.to_json

    elsif sequence =="1"
        s_params=Hash.new
        s_params['SEQUENCE']=sequence
        s_params['REQUEST_TYPE']=STRRESPONSE
        s_params['USSD_BODY']=ussd_body.strip #to remove spaces

        if ussd_body=="1"  #Register
          #checkTermConditions=TermsConditions.where(mobile_number: mobile).exists?
          checkRegistrationStatus=Registration.where(mobile_number: mobile).exists?
          
          if checkRegistrationStatus == true
              s_params['USSD_BODY']="You Are Already Registered To This Service,Thank You!"
              s_params['END_OF_SESSION']=STRTRUE
              
          #elsif checkTermConditions == false
            #  s_params['USSD_BODY']="You Cannot Register Without Accepting Terms And Conditions.\n Select Option 6 From Main Menu"
             # s_params['END_OF_SESSION']=STRTRUE
          else
              s_params['USSD_BODY']="Enter Your First Name"
              s_params['END_OF_SESSION']=STRFALSE
          end
              p s_params.to_json

         elsif ussd_body=="2"   #speak to a rep
            #checkRegistrationStatus=Register.where(mobile_number: mobile).exists?
            #if checkRegistrationStatus==true
              #user = Registration.where(mobile_number: mobile, is_registered: 1).pluck(:name)
              #user = user[0]
            #s_params['USSD_BODY']="Account Details\n Name: #{user}  \n 1. Edit\n 2.Cancel"
            #s_params['END_OF_SESSION']=STRFALSE
              #else
              s_params['USSD_BODY']="You Will Recieve A Text Message Shortly,Thank You!"
              s_params['END_OF_SESSION']=STRTRUE
              #end
            p s_params.to_json

         elsif ussd_body=="3"  #check balance
              #checkBalance hash
              accno=getAccNum(mobile)
              date=Time.now.strftime("%Y-%m-%dT%H:%M:%S")
              checkBal={
                            "balanceCheckRequest":{
                                "username":"",
                                "password":"",
                                "parameters":{
                                  "transactionId":"OOP",
                                  "accountNumber":accno,
                                  "accountBranch":"qwert",
                                  "requestDateTime":date
                               }
                             } 
                          }
              
              json_payload_check = checkBal.to_json
              bal=getBalance(mobile,json_payload_check)
              s_params['END_OF_SESSION']=STRTRUE
              p s_params.to_json


          elsif ussd_body=="4" #fund deposits
              s_params['USSD_BODY']="Transfer Funds\n \n 1. From Airtel to Air Wallet \n 2. From Air Wallet to Airtel \n 3.From Air Wallet to Air Wallet"
              s_params['END_OF_SESSION']=STRFALSE
              p s_params.to_json


         elsif ussd_body=="5" #withdrawal
              #nsano API function here
              s_params['USSD_BODY']="Enter Amount To Withdraw"
              s_params['END_OF_SESSION']=STRFALSE
              p s_params.to_json
         end
    elsif sequence=="2"
          s_params=Hash.new
          s_params['SEQUENCE']=sequence
          s_params['REQUEST_TYPE']=STRRESPONSE
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY']=ussd_body.strip
        if ussd_body == "1"
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY']="Transfer Funds From Airtel to Air Wallet. \n Please Enter Amount:"
        elsif ussd_body == "2"
          s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="Transfer Funds From Air Wallet to Airtel."
        elsif ussd_body == "3"
          s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="Transfer Funds From Air Wallet to Air Wallet."
        elsif ussd_body == "7"  
          allowTermsConditions(mobile)
          s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="Accepted Terms And Conditions.\n You Can Register Now."
        elsif ussd_body == "8"
          s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="You Did Not Accept Terms And Conditions, Thank You."
        elsif ussd_body.scan(/[aeiouAEIOU]/i).length == 0
          s_params = Hash.new
          s_params['SEQUENCE']= sequence
          s_params['REQUEST_TYPE'] = STRRESPONSE
          s_params['END_OF_SESSION'] = STRTRUE
          s_params['USSD_BODY'] = "Your Transaction Is In Progress.."
        else
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY']="#{ussd_body}, Enter Your Last Name"
        end
        p s_params.to_json
    elsif sequence=="3"
        if ussd_body.scan(/[aeiouAEIOU]/i).length == 0
          s_params = Hash.new
          s_params['SEQUENCE']= sequence
          s_params['REQUEST_TYPE'] = STRRESPONSE
          s_params['END_OF_SESSION'] = STRTRUE
          s_params['USSD_BODY'] = "Your Transaction Is In Progress.."
          task=Thread.new{
          msgHash={ merchant_number: STRMRCHNT, mobile_number: mobile, amount: ussd_body, reference: 'AirWallet', nickname: STRNICKNME }
          response=mobilePayment(msgHash)
          puts response.inspect
           if response[0]=="200" then
            saveTransactions(mobile,STRMRCHNT,ussd_body)
          else
            logger.info response.inspect
           end
        }
      else
          s_params = Hash.new
          s_params['SEQUENCE']= sequence
          s_params['REQUEST_TYPE'] = STRRESPONSE
          s_params['END_OF_SESSION'] = STRFALSE
          s_params['USSD_BODY'] = "Select ID Type\n \n 1. Passport \n 2. Voter ID \n 3. NIC \n 4. NHIS"
        end
      
          p s_params.to_json
          
    elsif sequence == "4"
          if ussd_body == "1" #passport
              s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRFALSE
                  s_params['USSD_BODY'] = "Enter Passport Number" 
            
            
            
            
            elsif ussd_body == "2" #voterID
            s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRFALSE
                  s_params['USSD_BODY'] = "Enter Voter ID Number" 
            
            
            
            
            
            elsif ussd_body == "3" #nia
            s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRFALSE
                  s_params['USSD_BODY'] = "Enter NIC Number" 
            
           else
             #nhis
            s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRFALSE
                  s_params['USSD_BODY'] = "Enter NHIS Number" 
          end
            p s_params.to_json
            
       elsif sequence == "5"
        getAll(service_key,sessionID,ussd_body,mobile)
        s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRFALSE
                  s_params['USSD_BODY'] = "Do you have an HFC bank account? \n1. Yes \n2. No"
                  p s_params.to_json
                  
        elsif sequence == "6"
          if ussd_body == "1" #user is an HFC account holder
              s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRFALSE
                  s_params['USSD_BODY'] = "Please Enter Your Account Number:" 
            
           elsif ussd_body == "2" #new user...send sms
            s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRTRUE
                  s_params['USSD_BODY'] = "You Will Recieve A Text Message Shortly.\n Thank You." 
           end
            p s_params.to_json
            
       elsif sequence == "7"
              updatePreReg(mobile)
              insertNewReg(mobile,ussd_body)
              s_params = Hash.new
              s_params['SEQUENCE']= sequence
              s_params['REQUEST_TYPE'] = STRRESPONSE
              s_params['END_OF_SESSION'] = STRTRUE
              s_params['USSD_BODY'] = "Account Successfully Created With Account Number #{ussd_body}" 
              p s_params.to_json
              #do prereg update here and reg insert here
            
    end
  end


  def getAccNum(mobile_number)
     accno = RegisteredUser.where(mobile_number: mobile_number).pluck(:acc_number)
     return accno
  end


  def saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
     ussdObject = UssdRequest.new
     ussdObject.sequence = sequence
     ussdObject.service_key = service_key
     ussdObject.session_id = sessionID
     ussdObject.mobile_number = mobile_number
     ussdObject.ussd_body = ussd_body
     ussdObject.created_at = Time.now
     ussdObject.updated_at = Time.now
     ussdObject.save
  end
  
  
  def getAll(service_key,session_id,ussd_body,mobile)
    first_name = UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?",service_key,session_id,mobile, '2').pluck(:ussd_body)
    first_name = first_name[0]
    last_name = UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?",service_key,session_id,mobile, '3').pluck(:ussd_body)
    last_name = last_name[0]
    id_type = UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?",service_key,session_id,mobile, '4').pluck(:ussd_body)
    id_type = id_type[0]
    id_number = ussd_body
    registrationObject = Registration.new
    registrationObject.name = first_name + " " + last_name
    registrationObject.mobile_number = mobile
    registrationObject.is_registered = 0
    registrationObject.id_type = id_type
    registrationObject.id_number = id_number
    registrationObject.created_at = Time.now
    registrationObject.updated_at = Time.now
    registrationObject.save
  end

  def saveTransactions(mobile_number,merchant_number,amount)
    transactionObject = Transaction.new
    transactionObject.amount = amount
    transactionObject.mobile_number = mobile_number
    transactionObject.status= 0
    transactionObject.merchant_number = merchant_number
    transactionObject.created_at= Time.now
    transactionObject.save
  end


  def saveTransactionResponse(mobile_number,merchant_number,amount,resp_code,res_description)
    responseObject = TransactionResponse.new
    responseObject.amount = amount
    responseObject.mobile_number = mobile_number
    responseObject.status= 0
    responseObject.merchant_number = merchant_number
    responseObject.created_at = Time.now
    responseObject.save
    logger.info responseObject.inspect
  end

  def allowTermsConditions(mobile_number)
    termsAndCons = TermsConditions.new
    termsAndCons.mobile_number = mobile_number
    termsAndCons.status = 1
    termsAndCons.save
  end
  
  
  
  def updatePreReg(mobile_number)
    Registration.update(mobile_number, is_registered: 1)
    #UPDATE addresses set user_name = 'Samuel' where cid = 15
  end
    
  def insertNewReg(mobile_number,account_number)
    regObject = RegisteredUser.new
    regObject.mobile_number = mobile_number
    regObject.is_registered = 1
    regObject.created_at = Time.now
    regObject.updated_at = Time.now
    regObject.acc_number = account_number
    regObject.save
  end
  
  
  def mobilePayment(msgHash)
   client = Savon.client(wsdl: 'http://41.190.91.198:10088/wservices.php?wsdl')
   response = client.call(:purchase_info, message: msgHash)
   resp=""
   resp=response.body[:purchase_info_response][:response]
   respArr=resp.split("~")
  end