  require 'sinatra'
  require 'active_record'
  require 'json'
  require 'net/http'
  require 'savon'

  STRRESPONSE = "RESPONSE"
  STRFALSE = "FALSE"
  STRTRUE = "TRUE"
  SEQUENCE = "SEQUENCE"
  STRMRCHNT = "233261061441"
  STRNICKNME = "HFC"
  STRTRMSNCONDTNS =  "By Reading This,You Agree On Terms And Conditions.\n 7. Yes \n 8.No"
  STRWRONGOPTION = "Wrong option selected. Please try again"

  ActiveRecord::Base.establish_connection(
    :adapter  => "mysql2",
    :host     => "localhost",
    :username => "jude",
    :password => "$jude1",
    :database => "hfc_test"
  )


  class TermsConditions < ActiveRecord::Base
    self.table_name = 'terms_conditions'
  end

  class UssdRequest < ActiveRecord::Base
   self.table_name = 'hfc_ussd_requests'
  end


  class Registration < ActiveRecord::Base
  self.table_name = 'hfc_registrations'
  has_many :registered_users
  end

  class RegisteredUser < ActiveRecord::Base
  self.table_name= 'hfc_registered_users'
  belongs_to :registration
  has_many :transactions
  end

  class Transaction < ActiveRecord::Base
  self.table_name= 'transactions'
  belongs_to :registered_user
  end


  class TransactionResponse < ActiveRecord::Base
  self.table_name= 'transaction_responses'
  end

  post '/' do
       request.body.rewind
       @payload=JSON.parse(request.body.read)
       json_vals = @payload
       sequence=json_vals['SEQUENCE']
       end_session=json_vals['END_OF_SESSION']
       sessionID=json_vals['SESSION_ID']
       service_key=json_vals['SERVICE_KEY']
       mobile_number=json_vals['MOBILE_NUMBER']
       ussd_body=json_vals['USSD_BODY'].strip if json_vals.has_key?('USSD_BODY')
       saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
       process(sequence,service_key,sessionID,mobile_number,ussd_body,end_session)
  end



  def process(sequence, service_key, sessionID, mobile, ussd_body, end_of_session='True')
    if sequence=="0"
     s_params=Hash.new
     s_params['SEQUENCE']=sequence
       s_params['REQUEST_TYPE']=STRRESPONSE
       s_params['END_OF_SESSION']=STRFALSE
       s_params['USSD_BODY']=" HFC Air Wallet\n\n 1. Register \n 2. More About Air Wallet \n 3. Speak To A Representative\n 4. Check Balance\n 5. Deposit Funds\n 6. Terms And Conditions"
       p s_params.to_json

    elsif sequence =="1"
        s_params=Hash.new
        s_params['SEQUENCE']=sequence
        s_params['REQUEST_TYPE']=STRRESPONSE
        s_params['USSD_BODY']=ussd_body.strip #to remove spaces

        if ussd_body=="1"  #Register
          checkTermConditions=TermsConditions.where(mobile_number: mobile).exists?
          checkRegistrationStatus=Registration.where(mobile_number: mobile).exists?
          
          if checkRegistrationStatus == true
              s_params['USSD_BODY']="You Are Already Registered To This Service,Thank You!"
              s_params['END_OF_SESSION']=STRTRUE
              
          elsif checkTermConditions == false
              s_params['USSD_BODY']="You Cannot Register Without Accepting Terms And Conditions.\n Select Option 6 From Main Menu"
              s_params['END_OF_SESSION']=STRTRUE
          else
              s_params['USSD_BODY']="Enter Your First Name"
              s_params['END_OF_SESSION']=STRFALSE
          end
              p s_params.to_json

         elsif ussd_body=="2"   #get more of airwallet

            #checkRegistrationStatus=Register.where(mobile_number: mobile).exists?
            #if checkRegistrationStatus==true
              #user = Registration.where(mobile_number: mobile, is_registered: 1).pluck(:name)
              #user = user[0]
            #s_params['USSD_BODY']="Account Details\n Name: #{user}  \n 1. Edit\n 2.Cancel"
            #s_params['END_OF_SESSION']=STRFALSE
              #else
              s_params['USSD_BODY']="You Will Recieve A Text Message Shortly,Thank You!"
              s_params['END_OF_SESSION']=STRTRUE
              #end
            p s_params.to_json

         elsif ussd_body=="3"  #speak to a representative

            #checkRegistrationStatus=Register.where(mobile_number: mobile).exists?
            #if checkRegistrationStatus==true
              #user = Registration.where(mobile_number: mobile, is_registered: 1).pluck(:name)
              #user = user[0]
            #s_params['USSD_BODY']="Account Details\n Name: #{user}  \n 1. Edit\n 2.Cancel"
            #s_params['END_OF_SESSION']=STRFALSE
              #else
              s_params['USSD_BODY']="You Will Recieve A Text Message Shortly,Thank You!"
              s_params['END_OF_SESSION']=STRTRUE
              #end
            p s_params.to_json


          elsif ussd_body=="4" #check balance
              s_params['USSD_BODY']="Your Available Balance Is GHS 0.00"
              s_params['END_OF_SESSION']=STRTRUE
                p s_params.to_json

         elsif ussd_body=="5" #transfer funds
              s_params['USSD_BODY']="Transfer Funds\n \n 1. From Airtel to Air Wallet \n 2. From Air Wallet to Airtel \n 3.From Air Wallet to Air Wallet"
              s_params['END_OF_SESSION']=STRFALSE
              p s_params.to_json

        elsif  ussd_body == "6" #read terms and conditions
             s_params['USSD_BODY']= STRTRMSNCONDTNS
             s_params['END_OF_SESSION']=STRFALSE
             p s_params.to_json
             
         end




    elsif sequence=="2"
          s_params=Hash.new
          s_params['SEQUENCE']=sequence
          s_params['REQUEST_TYPE']=STRRESPONSE
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY']=ussd_body.strip
        if ussd_body == "1"
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY']="Transfer Funds From Airtel to Air Wallet. \n Please Enter Amount."
        elsif ussd_body == "2"
          s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="Transfer Funds From Air Wallet to Airtel."
        elsif ussd_body == "3"
          s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="Transfer Funds From Air Wallet to Air Wallet."
        elsif ussd_body == "7"  
          allowTermsConditions(mobile)
          s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="Accepted Terms And Conditions.\n You Can Register Now."
          elsif ussd_body == "8"
          s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="You Did Not Accept Terms And Conditions, Thank You."
        else
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY']="#{ussd_body}, Enter Your Last Name"
          end
        p s_params.to_json


    elsif sequence=="3"
        if ussd_body.scan(/[aeiouAEIOU]/i).length == 0
         
          s_params = Hash.new
          s_params['SEQUENCE']= sequence
          s_params['REQUEST_TYPE'] = STRRESPONSE
          s_params['END_OF_SESSION'] = STRTRUE
          s_params['USSD_BODY'] = "Your Transaction Is In Progress.."
          
          
          task=Thread.new{
          msgHash={ merchant_number: STRMRCHNT, mobile_number: mobile, amount: ussd_body, reference: 'AirWallet', nickname: STRNICKNME }
          response=mobilePayment(msgHash)
          puts response.inspect
           if response[0]=="200" then
            saveTransactions(mobile,STRMRCHNT,ussd_body)
          else
            logger.info response.inspect
           end
        }
          
      else
            s_params = Hash.new
          s_params['SEQUENCE']= sequence
          s_params['REQUEST_TYPE'] = STRRESPONSE
          s_params['END_OF_SESSION'] = STRFALSE
          s_params['USSD_BODY'] = "Select ID Type\n \n 1. Passport \n 2. Voter ID \n 3. NIC \n 4. NHIS"
        end
      
          p s_params.to_json
          
    elsif sequence == "4"
          if ussd_body == "1" #passport
              s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRFALSE
                  s_params['USSD_BODY'] = "Enter Passport Number" 
            
            
            
            
            elsif ussd_body == "2" #voterID
            s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRFALSE
                  s_params['USSD_BODY'] = "Enter Voter ID Number" 
            
            
            
            
            
            elsif ussd_body == "3" #nia
            s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRFALSE
                  s_params['USSD_BODY'] = "Enter NIC Number" 
            
           else
             #nhis
            s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRFALSE
                  s_params['USSD_BODY'] = "Enter NHIS Number" 
            
            
            
          end
            p s_params.to_json
            
        elsif sequence == "5"
        getAll(service_key,sessionID,ussd_body,mobile)
        s_params = Hash.new
                  s_params['SEQUENCE']= sequence
                  s_params['REQUEST_TYPE'] = STRRESPONSE
                  s_params['END_OF_SESSION'] = STRTRUE
                  s_params['USSD_BODY'] = "Thanks For Registering With Us"
            
                  p s_params.to_json
    end
  end





  def saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
    ussdObject = UssdRequest.new
     ussdObject.sequence = sequence
     ussdObject.service_key = service_key
     ussdObject.session_id = sessionID
     ussdObject.mobile_number = mobile_number
     ussdObject.ussd_body = ussd_body
     ussdObject.created_at = Time.now
     ussdObject.updated_at = Time.now
     ussdObject.save
  end
  
  
  def getAll(service_key,session_id,ussd_body,mobile)
    first_name = UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?",service_key,session_id,mobile, '2').pluck(:ussd_body)
    first_name = first_name[0]
    last_name = UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?",service_key,session_id,mobile, '3').pluck(:ussd_body)
    last_name = last_name[0]
    id_type = UssdRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?",service_key,session_id,mobile, '4').pluck(:ussd_body)
    id_type = id_type[0]
    id_number = ussd_body
    registrationObject = Registration.new
      registrationObject.name = first_name + " " + last_name
    registrationObject.mobile_number = mobile
    registrationObject.is_registered = 0
    registrationObject.id_type = id_type
    registrationObject.id_number = id_number
    registrationObject.created_at = Time.now
      registrationObject.updated_at = Time.now
    registrationObject.save
  end

  def saveTransactions(mobile_number,merchant_number,amount)
    transactionObject = Transaction.new
    transactionObject.amount = amount
    transactionObject.mobile_number = mobile_number
    transactionObject.status= 0
    transactionObject.merchant_number = merchant_number
    transactionObject.created_at= Time.now
    transactionObject.save
  end


  def saveTransactionResponse(mobile_number,merchant_number,amount,resp_code,res_description)
    responseObject = TransactionResponse.new
    responseObject.amount = amount
    responseObject.mobile_number = mobile_number
    responseObject.status= 0
    responseObject.merchant_number = merchant_number
    responseObject.created_at = Time.now
    responseObject.save
    logger.info responseObject.inspect
  end

  def allowTermsConditions(mobile_number)
    termsAndCons = TermsConditions.new
    termsAndCons.mobile_number = mobile_number
    termsAndCons.status = 1
    termsAndCons.save
  end



  def mobilePayment(msgHash)
   client = Savon.client(wsdl: 'http://41.190.91.198:10088/wservices.php?wsdl')
   response = client.call(:purchase_info, message: msgHash)
   resp=""
   resp=response.body[:purchase_info_response][:response]
   respArr=resp.split("~")
  end