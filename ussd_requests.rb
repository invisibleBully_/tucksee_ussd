require 'sinatra' 
require 'active_record' 
require 'json' 
require 'net/http'
require 'rack-flash'
require 'sinatra/redirect_with_flash'

ActiveRecord::Base.establish_connection(
  :adapter  => "mysql2",
  :host     => "localhost",
  :username => "jude",
  :password => "$jude1",
  :database => "jd_db"
)



class UssdRequest < ActiveRecord::Base
 self.table_name = 'ussd_requests'
end


class Registration < ActiveRecord::Base
self.table_name = 'registrations'
has_many :registered_users
end

class RegisteredUser < ActiveRecord::Base
self.table_name= 'registered_users'
belongs_to :registration
end



get '/' do
		@requests = UssdRequest.all :order => :id.desc
    @title = 'Requests  | USSD'
    if @requests.empty?
        flash[:error] = 'No Requests Found'
    end
    erb :home
end 


get '/confirmed_users'
	@title= "Confirmations"
end


get '/registrations'
	@title= "User Registrations"
end


