require 'sinatra' 
require 'active_record' 
require 'json' 
require 'net/http'

STRRESPONSE="RESPONSE"
STRFALSE="FALSE"
STRTRUE="TRUE"
SEQUENCE="SEQUENCE"


ActiveRecord::Base.establish_connection(
    :adapter  => "mysql2",
    :host     => "localhost",
    :username => "jude",
    :password => "$jude1",
    :database => "jd_db"
  )

  
STRRESPONSE = "RESPONSE"
STRFALSE = "FALSE"
STRTRUE = "TRUE"
SEQUENCE = "SEQUENCE"
STRMRCHNT = "233261064828"
STRNCKNAME = "APPSNMOBIL"

class USSDRequest < ActiveRecord::Base
 self.table_name = 'lkm_ussd_requests'
end


class LKMTransaction < ActiveRecord::Base
  self.table_name = 'lkm_transactions'
end


class LKMVal < ActiveRecord::Base
  self.table_name = 'lkm_verify_vals'
end




post '/lekma_remote_resp' do
    

end





post '/' do
		 request.body.rewind
		 @payload=JSON.parse(request.body.read)
		 json_vals = @payload
	 	 sequence=json_vals['SEQUENCE']
	   end_session=json_vals['END_OF_SESSION']
	   sessionID=json_vals['SESSION_ID']
	   service_key=json_vals['SERVICE_KEY']
	   mobile_number=json_vals['MOBILE_NUMBER']
	   ussd_body=json_vals['USSD_BODY'].strip if json_vals.has_key?('USSD_BODY')
	   saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
	   process(sequence,service_key,sessionID,mobile_number,ussd_body,end_session)
end



def process(sequence, service_key, sessionID, mobile, ussd_body, end_of_session='True')
  
  if sequence=="0"
     s_params=Hash.new
   	 s_params['SEQUENCE']=sequence
		 s_params['REQUEST_TYPE'] = STRRESPONSE
		 s_params['END_OF_SESSION'] = STRFALSE
		 s_params['USSD_BODY'] = "Pay bills via Tucksee Pay \n 1. Tucksee Pay \n 2. Other"  
		 p s_params.to_json
	   
	elsif sequence == "1"
	     	s_params=Hash.new
			  s_params['SEQUENCE'] = sequence
		   	s_params['REQUEST_TYPE']=STRRESPONSE
		   	s_params['USSD_BODY']=ussd_body.strip #to remove spaces 
		   	
		   	if ussd_body == "1"
			   	s_params['END_OF_SESSION']=STRFALSE
	        s_params['USSD_BODY'] = "Please Enter PRN: \n"
			  elsif ussd_body == "2"
			    s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="Other Payment Methods Not Available. Try again later"  
			  end
			    p s_params.to_json
	elsif sequence=="2"
				
				_prn = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '1').pluck(:ussd_body)
    		#_strbody = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '1').pluck(:ussd_body)
		
				s_params=Hash.new
				s_params['SEQUENCE']=sequence
		   	s_params['REQUEST_TYPE']=STRRESPONSE
		   	s_params['END_OF_SESSION']=STRFALSE
		   	s_params['USSD_BODY']=ussd_body.strip
		   	
		   	if ussd_body=="1"
		   		s_params['END_OF_SESSION']=STRFALSE
		   		s_params['USSD_BODY']="Edit Name."
		   	elsif ussd_body=="2"
		   		s_params['END_OF_SESSION']=STRTRUE
		   		s_params['USSD_BODY']="Cancel"
		   	else
		   	  saveRegistrations(ussd_body,mobile)                                                     
		   		s_params['END_OF_SESSION']=STRTRUE
		   		s_params['USSD_BODY']="Congrats #{ussd_body}, you have sucessfully registered."
				end 
			  p s_params.to_json
		elsif sequence=="3"
				s_params=Hash.new
			  s_params['SEQUENCE']=sequence
		   	s_params['REQUEST_TYPE']=STRRESPONSE
		   	s_params['END_OF_SESSION']=STRTRUE
		   	editUserName(ussd_body,mobile)																											
		   	s_params['USSD_BODY']=" Name Changed To, #{ussd_body}" 
			  p s_params.to_json
		end
end





def saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
	 ussdObject = USSDRequest.new
   ussdObject.sequence = sequence
   ussdObject.service_key = service_key
   ussdObject.session_id = sessionID
   ussdObject.mobile_number = mobile_number
   ussdObject.ussd_body = ussd_body
   ussdObject.created_at = Time.now
   ussdObject.updated_at = Time.now
   ussdObject.save
end



