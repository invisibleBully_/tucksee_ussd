  require 'sinatra'
  require 'active_record'
  require 'json'
  require 'net/http'
  require 'savon'
  #require './apis'
  #require 'faraday'

  
  STRRESPONSE = "RESPONSE"
  STRFALSE = "FALSE"
  STRTRUE = "TRUE"
  SEQUENCE = "SEQUENCE"



  #class UssdRequest < ActiveRecord::Base
   #self.table_name = 'lkm_ussd_requests'
  #end

  post '/' do
       request.body.rewind
       @payload=JSON.parse(request.body.read)
       json_vals = @payload
       sequence=json_vals['SEQUENCE']
       end_session=json_vals['END_OF_SESSION']
       sessionID=json_vals['SESSION_ID']
       service_key=json_vals['SERVICE_KEY']
       mobile_number=json_vals['MOBILE_NUMBER']
       ussd_body=json_vals['USSD_BODY'].strip if json_vals.has_key?('USSD_BODY')
       #saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
       process(sequence,service_key,sessionID,mobile_number,ussd_body,end_session)
  end



  def process(sequence, service_key, sessionID, mobile, ussd_body, end_of_session='True')
    if sequence=="0"
        s_params = Hash.new
        s_params['SEQUENCE']=sequence
        s_params['REQUEST_TYPE']=STRRESPONSE
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY']="WAEC \n\n 1. Services \n 2. Fees"
        p s_params.to_json
    elsif sequence == "1"
        s_params = Hash.new
        s_params['SEQUENCE'] = sequence
        s_params['REQUEST_TYPE'] = STRRESPONSE
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY'] = ussd_body.strip
        if ussd_body == "1"
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY']="WAEC SERVICES \n \n 1. Result Confirmation \n 2. Result Verification \n 3.Certificate Request \n 4. Result Attestation"
        else
        s_params['END_OF_SESSION']=STRTRUE
        s_params['USSD_BODY']="This service is currently unavailable."
      end
          p s_params.to_json
    elsif sequence == "2"
          s_params=Hash.new
          s_params['SEQUENCE'] = sequence
          s_params['REQUEST_TYPE'] = STRRESPONSE
          s_params['END_OF_SESSION'] = STRFALSE
           if ussd_body == "1" #result confirmation
                s_params['END_OF_SESSION']=STRTRUE
                s_params['USSD_BODY']="This service is currently unavailable."
               # p s_params.to_json
          elsif ussd_body == "2"
                s_params['END_OF_SESSION']=STRTRUE
                s_params['USSD_BODY']="This service is currently unavailable."
          elsif ussd_body == "3"
                s_params['END_OF_SESSION']=STRTRUE
                s_params['USSD_BODY']="This service is currently unavailable."
           elsif ussd_body == "4"
                s_params['END_OF_SESSION']=STRTRUE
                s_params['USSD_BODY']="This service is currently unavailable."
         end
          p     s_params.to_json
    elsif sequence == "3"
          s_params=Hash.new
          s_params['SEQUENCE']=sequence
          s_params['REQUEST_TYPE']=STRRESPONSE
          s_params['END_OF_SESSION']=STRFALSE
          if ussd_body == "1" #confirm[transact and close transaction]
               
                s_params['END_OF_SESSION']=STRTRUE
                s_params['USSD_BODY']="Your transaction failed to process.Try again late.\n Thank you."
                p s_params.to_json
               
          else
            s_params['END_OF_SESSION']=STRTRUE
            s_params['USSD_BODY']="Your transaction is being processed."
         end
           p s_params.to_json
    elsif sequence == "4"
          s_params=Hash.new
          s_params['SEQUENCE']=sequence
          s_params['REQUEST_TYPE']=STRRESPONSE
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY'] = ussd_body.strip
       if ussd_body == "1"
         #call transaction and close
         s_params['END_OF_SESSION']=STRTRUE
         s_params['USSD_BODY']="Your transaction has been cancelled."
        end
        p s_params.to_json
    end
  end


