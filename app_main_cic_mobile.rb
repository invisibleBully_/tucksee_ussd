require 'savon'
require 'active_record'
require 'json'
require 'net/http'
require 'faraday'
require 'digest/md5'
require 'openssl'
require 'nokogiri'
require 'base64'
require 'csv'


STRRESPONSE = "RESPONSE"
STRFALSE = "FALSE"
STRTRUE = "TRUE"
SEQUENCE = "SEQUENCE"
STRMRCHNT = "233261064828"
STRNCKNAME = "APPSNMOBIL"
REFERENCE = 'WAEC' 
RSLT_CHK = 'RCH'
STROPENTIMEOUT="180"
new_url     =  'https://appsnmobileagent.com:8201'
new_HEADERS = {'Content-Type'=> 'application/json','timeout'=>STROPENTIMEOUT, 'open_timeout'=>STROPENTIMEOUT}
CLIENT_KEY = 'i2ANQ0lLLxzLVeycm+s6u/P86GXPttoJAORHoxexomqe9Th5kpo+0lVjUKGSlFovCx41wRb7mxGJvYineBdqew=='
SECRET_KEY = 'QiJ8fhF47EDy8Hdi5iq9Vu+boGu8o5thXNhW1Ji1kYCCLdnf03GBoXa6Ip8qWXxy1KQnlxHp6tGXmswNGIMFng=='
AUTH_CODE = 'H3bLvqTPlsI81c5cICAze6AXa9FAb5Zrkbt1mPY2Pmc='
CLIENT = Savon.client(wsdl: 'http://waec.universalbusinesstechnology.com/bankpayment/?wsdl')
AMOUNT = 7.5
NAME = 'Jude Botchwey'



NEW_CONN = Faraday.new(:url => new_url, :headers => new_HEADERS, :ssl => {:verify => false}) do |faraday|   
  faraday.request  :url_encoded             # form-encode POST params
  faraday.response :logger                  # log requests to STDOUT
  faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
end


ActiveRecord::Base.establish_connection(
:adapter  => "mysql2",
:host     => "67.205.74.208",
:username => "jude",
:password => "Tr1ad@12",
:database => "cic_mobile"
)

class USSDRequest < ActiveRecord::Base
  self.table_name = 'ussd_requests'
end



class StudentRquest < ActiveRecord::Base
  self.table_name = 'student_requests'
end

class RequestType < ActiveRecord::Base
  self.table_name = 'request_types'
end




post '/' do
  request.body.rewind
  @payload=JSON.parse(request.body.read)
  json_vals = @payload
  sequence=json_vals['SEQUENCE']
  end_session=json_vals['END_OF_SESSION']
  sessionID=json_vals['SESSION_ID']
  service_key=json_vals['SERVICE_KEY']
  mobile_number=json_vals['MOBILE_NUMBER']
  ussd_body=json_vals['USSD_BODY'].strip if json_vals.has_key?('USSD_BODY')
  saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
  process(sequence,service_key,sessionID,mobile_number,ussd_body,end_session)
end


def process(sequence, service_key, sessionID, mobile, ussd_body, end_of_session='True')
  if sequence=="0"
    s_params = Hash.new
    s_params['SEQUENCE']=sequence
    s_params['REQUEST_TYPE']=STRRESPONSE
    s_params['END_OF_SESSION']=STRFALSE
    s_params['USSD_BODY']= "CIC Mobile \n\n 1. Banking \n 2. Loans \n 3. Value Added Services \n"
    p s_params.to_json
  elsif sequence == "1"
    s_params = Hash.new
    s_params['SEQUENCE'] = sequence
    s_params['REQUEST_TYPE'] = STRRESPONSE
    s_params['END_OF_SESSION']=STRFALSE
    s_params['USSD_BODY'] = ussd_body.strip
       
    if ussd_body == "1"
      s_params['END_OF_SESSION'] = STRFALSE
      s_params['USSD_BODY']="\n Banking \n\n 1.Subscribe to service \n 2. Balance Enquiry \n 3. Account Statement \n 4. Transfer Funds \n 5. Customer Service \n 6. Change PIN"
    elsif ussd_body == "2"
      s_params['END_OF_SESSION'] = STRFALSE
      s_params['USSD_BODY']="\n Loans \n\n 1. Apply for loan"
    elsif ussd_body == "3"
      s_params['END_OF_SESSION'] = STRFALSE
      s_params['USSD_BODY']="\n Value Added Services \n\n 1. Purchase Airtime \n 2. Bill Payment"
    else
      s_params['END_OF_SESSION'] = STRTRUE
      s_params['USSD_BODY']="You selected a wrong option.Please try again."
    end
    p s_params.to_json
    
  elsif sequence == "2"
    strbody = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '1').pluck(:ussd_body)
  
    s_params = Hash.new
    s_params['SEQUENCE'] = sequence
    s_params['REQUEST_TYPE'] = STRRESPONSE
    s_params['END_OF_SESSION']=STRFALSE
    s_params['USSD_BODY'] = ussd_body.strip

    if strbody[0].strip == '1' #Banking
      if ussd_body == '1' #Subscribe to service
        s_params['END_OF_SESSION'] = STRTRUE
        s_params['USSD_BODY']="\n Subscribe to Service \n\n Sorry, this service is currently unavailable"
      elsif   ussd_body == '2'#balance enquiry
        s_params['END_OF_SESSION'] = STRTRUE
        s_params['USSD_BODY']="\n Balance Enquiry \n\n Sorry, this service is currently unavailable"
      elsif ussd_body == '3' #Account statement
        s_params['END_OF_SESSION'] = STRFALSE
        s_params['USSD_BODY']="\n Account Statement \n\n 1. Mini Statement \n 2. Full Statement"
      elsif ussd_body == '4' #Transfer funds
        s_params['END_OF_SESSION'] = STRTRUE
        s_params['USSD_BODY']="\n Transfer Funds \n\n Sorry, this service is currently unavailable"
      elsif ussd_body == '5' #Customer service
        s_params['END_OF_SESSION'] = STRTRUE
        s_params['USSD_BODY']="Customer Service \n\n Sorry, this service is currently unavailable"
      elsif ussd_body == '6' #Change PIN
        s_params['END_OF_SESSION'] = STRTRUE
        s_params['USSD_BODY']="Change PIN \n\n Sorry, this service is currently unavailable"
      end
    elsif strbody[0].strip == '2' 
     if ussd_body == '1' #Loans Application side
        s_params['END_OF_SESSION'] = STRTRUE
        s_params['USSD_BODY']="Apply for Loan \n\n Sorry, this service is currently unavailable.Try again later"
      end
    elsif strbody[0].strip == '3' #value added services --options
     if ussd_body == '1'
        s_params['END_OF_SESSION'] = STRTRUE
        s_params['USSD_BODY']="\n Purchase Airtime \n\n Sorry, this service is currently unavailable.Try again later"
      elsif ussd_body == '2'
        s_params['END_OF_SESSION'] = STRTRUE
        s_params['USSD_BODY']="\n Bill Payment \n\n Sorry, this service is currently unavailable.Try again later"
      end
    end
    p  s_params.to_json
  elsif sequence == "3"

    strbody = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '1').pluck(:ussd_body)
    s_params = Hash.new
    s_params['SEQUENCE'] = sequence
    s_params['REQUEST_TYPE'] = STRRESPONSE
    s_params['END_OF_SESSION']=STRFALSE
    s_params['USSD_BODY'] = ussd_body.strip

    if strbody[0].strip=='1'
        if ussd_body == '1' #ministatement
         s_params['END_OF_SESSION']=STRTRUE
         s_params['USSD_BODY']="\n Mini Statement  \n\n Service is currently unavailable"
        elsif ussd_body == '2' #fullstatement
           s_params['END_OF_SESSION']=STRTRUE
         s_params['USSD_BODY']="\n Full Statement \n\n Service is currently unavailable"
        end
    else
      s_params['END_OF_SESSION']=STRTRUE
      s_params['USSD_BODY']="\n Sorry, try again later"
    end
    p  s_params.to_json

  elsif sequence == "4"
    strbody = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '1').pluck(:ussd_body)

    s_params=Hash.new
    s_params['SEQUENCE']=sequence
    s_params['REQUEST_TYPE']=STRRESPONSE
    s_params['END_OF_SESSION']=STRFALSE
    s_params['USSD_BODY'] = ussd_body.strip

    p  s_params.to_json
  end
end

def saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
  ussdObject = USSDRequest.new
  ussdObject.sequence = sequence
  ussdObject.service_key = service_key
  ussdObject.session_id = sessionID
  ussdObject.mobile_number = mobile_number
  ussdObject.ussd_body = ussd_body
  ussdObject.created_at = Time.now
  ussdObject.save
end


def genUniqueCodeCredit
  day= Time.new.strftime("%d")
  month = Time.new.strftime("%m")
  year = Time.new.strftime("%y")
  hour = Time.new.strftime("%H")
  minute = Time.new.strftime("%M")
  second = Time.new.strftime("%S")
  first_two = rand(00..99)
  first_two = first_two.to_s.rjust(2, "0")
  uniq_id="#{RSLT_CHK}#{year}#{first_two}#{month}#{day}#{hour}#{minute}#{second}"
  return uniq_id
end


def mobileMoneyPayment(merchant_no,nick_name,reference,mobile_number,amount,trnx_id,client_id,secret_key)

  endpoint = "/debitCustomerWallet"
  
  ts=Time.now.strftime("%Y-%m-%d %H:%M:%S")

  payload={ 
      :merchant_number=> merchant_no,
      :customer_number=> mobile_number,
      :amount=> amount,
      :reference=> reference,
      :exttrid=> trnx_id,
      :nickname=>nick_name,
      :ts=>ts 
      }

  json_payload=JSON.generate(payload)
  msg_endpoint="#{endpoint}#{json_payload}"
  
  puts
  puts msg_endpoint
  puts


  def computeSignature(secret, data)
      digest=OpenSSL::Digest.new('sha256')
      signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
      return signature
  end
 

  signature=computeSignature(secret_key, msg_endpoint)
  
  begin
  res=NEW_CONN.post do |req|
    req.url endpoint
    req.options.timeout = 30           # open/read timeout in seconds
    req.options.open_timeout = 30      # connection open timeout in seconds
    req["Authorization"]="#{client_id}:#{signature}"
    req.body = json_payload
  end
  
  
  
  rescue Faraday::SSLError
    puts
    puts "There was a problem sending the https request..."
    puts
  rescue Faraday::TimeoutError
    puts "Connection timeout error"
  end
  
end
  






