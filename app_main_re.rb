#require 'sinatra'
#require 'active_record'
#require 'json'
#require 'net/http'
#require 'savon'
require './apis'
#require 'faraday'



class UssdRequest < ActiveRecord::Base
   self.table_name = 'lkm_ussd_requests'
end

class TmpVal < ActiveRecord::Base
  self.table_name = 'lkm_tmp_vals'
end


STRSCKEY =  "I7QWq2CVzddk1aZKKRw6uB5Qg7u1X/uY8VCOsKxOt+T8pXY6tLnR5GhzszUSz/TC1m4LoLQj1RyLn9I4IBMFsQ=="
STRPUBKEY = "+P7q7paQ4DnWfWtluZ5R+bdljlA779feZVr4ZeE5Er4fEo5NAX0g6xIgHSFR2bCHWL83SXT0WH0hq+ERRF1LOw=="
STRMRCHNUM = "261064828"
STRMRCHNICK = "APPSNMOB"




post '/lekma_remote_resp' do
  request.body.rewind
  req = JSON.parse request.body.read
  resp_code=req["trans_status"]
  resp_desc=req["message"]
  refID = req["trans_ref"]
  am_refID = req["trans_id"]
  
  
  if resp_code == '200' then 
    #dotransaction and close transaction
    
    else
    #payment failed do nothing...send notification to user
    
  end
  
  
  
end









post '/' do
   request.body.rewind
   @payload=JSON.parse(request.body.read)
   json_vals = @payload
   sequence=json_vals['SEQUENCE']
   end_session=json_vals['END_OF_SESSION']
   sessionID=json_vals['SESSION_ID']
   service_key=json_vals['SERVICE_KEY']
   mobile_number=json_vals['MOBILE_NUMBER']
   ussd_body=json_vals['USSD_BODY'].strip if json_vals.has_key?('USSD_BODY')
   saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
   process(sequence,service_key,sessionID,mobile_number,ussd_body,end_session)
end





def process(sequence, service_key, sessionID, mobile, ussd_body, end_of_session='True')
    if sequence=="0"
        s_params = Hash.new
        s_params['SEQUENCE']=sequence
        s_params['REQUEST_TYPE']=STRRESPONSE
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY']="Pay bills via Tucksee Pay\n\n 1. Tucksee Pay \n 2. Other Payment Method"
        p s_params.to_json
    elsif sequence == "1"
        s_params = Hash.new
        s_params['SEQUENCE'] = sequence
        s_params['REQUEST_TYPE'] = STRRESPONSE
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY'] = ussd_body.strip
        if ussd_body == "1"
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY']="Please Enter PRN: \n"
        else
        s_params['END_OF_SESSION']=STRTRUE
        s_params['USSD_BODY']="Other Payment Methods Not Available. Try again later"
      end
          p s_params.to_json
    elsif sequence == "2"
          s_params=Hash.new
          s_params['SEQUENCE'] = sequence
          s_params['REQUEST_TYPE'] = STRRESPONSE
          s_params['END_OF_SESSION'] = STRFALSE
          @payment_reference = "#{ussd_body}"
          
          auth_array = apiAuthenticate()
          
          @auth_session = auth_array[0]
          @auth_success = auth_array[1]
          
          if @auth_success == "1"
                verify_array_values = verifyReference(@auth_session,@payment_reference,mobile,sessionID)
                
                @verifySystemName = verify_array_values[0]
                @verify_payment_amount = verify_array_values[1]
                @verify_session = verify_array_values[2]
                @verify_success = verify_array_values[3]
                @verify_prn = verify_array_values[4]
                @verify_partial_value = verify_array_values[5]
                
                saveTmpVals(@verifySystemName,@verify_payment_amount,@verify_session,@verify_success,@verify_prn,@verify_partial_value,mobile,sessionID)
                #saveTemporalValues Here and collect in the other sequence
                
                
                 if @verify_partial_value == "0" then #go ahead and do transaction
                   s_params['USSD_BODY'] = "CONFIRM PAYMENT \n\n PRN: #{@payment_reference}  \n Account: #{@verifySystemName} \n Amount Due: #{@verify_payment_amount}  \n \n 1. Confirm \n 2. Cancel:"
                   s_params['END_OF_SESSION'] = STRFALSE
                   elsif @verify_partial_value == "1" #enter amount
                   s_params['END_OF_SESSION'] = STRFALSE
                   s_params['USSD_BODY'] = "Partial Payment\n\n Please Enter Amount:"
                 end
       
         else
               s_params['USSD_BODY'] = "There was a problem. Please try again later."
               s_params['END_OF_SESSION'] = STRTRUE
               p     s_params.to_json
         end 
          p     s_params.to_json
    elsif sequence == "3"
          s_params=Hash.new
          s_params['SEQUENCE']=sequence
          s_params['REQUEST_TYPE'] = STRRESPONSE
          s_params['END_OF_SESSION'] = STRFALSE
          
          if ussd_body == '1'
            #_the_params = LKMVal.where("session_id=? and mobile_number=?", sessionID, mobile).order("id desc").limit(1)
            ###post transaction to am
              #the_params = LKMVal.where("session_id=? and mobile_number=?", sessionID, mobile).order("id desc").limit(1)
                #sessionKay = the_params[0]["verify_session_val"]
                #verPayRef  = the_params[0]["verify_pay_ref"]
                #vrSuccCode = the_params[0]["verify_succ_code"]
                #verSysName = the_params[0]["verify_sys_name"]
                #verAmount = the_params[0]["verify_amount"]
                #verPartialValue = the_params[0]["ver_partial"]
                #paidDate = Time.new
            
            
            
            _trans_id = genUniqueCodeCredit
            
            
           _task = Thread.new{
              mobileMoneyPayment(STRMRCHNUM,STRMRCHNICK,reference,mobile_number,amount,_trans_id,STRPUBKEY,STRSCKEY)
            }
            
            s_params['USSD_BODY'] = "Your transaction is being processed. Wait for prompt to authorize your transaction"
            s_params['END_OF_SESSION'] = STRTRUE

            elsif ussd_body == '2'
            s_params['USSD_BODY'] = "You cancelled your transaction."
            s_params['END_OF_SESSION'] = STRTRUE
            
            else
            #########Pull Saved Temp Vals and show confirmation to  user to confirm
             _temp_array = TmpVal.where(session_id: sessionID,mobile: mobile)[0]
            
             s_params['USSD_BODY'] = "CONFIRM PAYMENT \n\n PRN: #{_temp_array.prn}  \n Account: #{_temp_array.sys_name} \n Amount Due: #{_temp_array.amount.to_f}  \n \n 1. Confirm \n 2. Cancel:"
             s_params['END_OF_SESSION'] = STRFALSE
            
          
          end         
          p s_params.to_json
    elsif sequence == "4"
          s_params=Hash.new
          s_params['SEQUENCE']=sequence
          s_params['REQUEST_TYPE']=STRRESPONSE
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY'] = ussd_body.strip
       if ussd_body == "1"
         #call transaction and close
         the_params = LKMVal.where("session_id=? and mobile_number=?", sessionID, mobile).order("id desc").limit(1)
         sessionKay = the_params[0]["verify_session_val"]
         verPayRef  = the_params[0]["verify_pay_ref"]
         vrSuccCode = the_params[0]["verify_succ_code"]
         verSysName = the_params[0]["verify_sys_name"]
         verAmount = the_params[0]["verify_amount"]
         
         if  vrSuccCode == "1"
           task = Thread.new{
             transact_array_values = transact(sessionKay,verPayRef,)
             transaction_session = transact_array_values[0]
             transaction_prn = transact_array_values[1]
             transactionID = transact_array_values[3]
             transaction_success = transact_array_values[2]
             
              if transaction_success == "1" 
              closeTransaction(transaction_session,transaction_prn,transactionID)
              end
              }
              s_params['END_OF_SESSION']=STRTRUE
              s_params['USSD_BODY']="Your transaction will be processed."
              p s_params.to_json
          else
            s_params['END_OF_SESSION']=STRTRUE
            s_params['USSD_BODY']="Function Error"
            p s_params.to_json
         end
        else
         s_params['END_OF_SESSION']=STRTRUE
         s_params['USSD_BODY']="Your transaction has been cancelled."
        end
        p s_params.to_json
    end
  end
























def mobileMoneyPayment(merchant_no,nick_name,reference,mobile_number,amount,trnx_id,client_id,secret_key)

  endpoint = "/debitCustomerWallet"
  
  ts=Time.now.strftime("%Y-%m-%d %H:%M:%S")

  payload={ 
      :merchant_number=> merchant_no,
      :customer_number=> mobile_number,
      :amount=> 0.1,
      :reference=> reference,
      :exttrid=> trnx_id,
      :nickname=>nick_name,
      :ts=>ts
      }

  json_payload=JSON.generate(payload)
  msg_endpoint="#{endpoint}#{json_payload}"
  
  puts
  puts msg_endpoint
  puts


  def computeSignature(secret, data)
      digest=OpenSSL::Digest.new('sha256')
      signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
      return signature
  end
 

  signature=computeSignature(secret_key, msg_endpoint)
  
  begin
  res=NEW_CONN.post do |req|
    req.url endpoint
    req.options.timeout = 30           # open/read timeout in seconds
    req.options.open_timeout = 30      # connection open timeout in seconds
    req["Authorization"]="#{client_id}:#{signature}"
    req.body = json_payload
  end
  
  
  
  rescue Faraday::SSLError
    puts
    puts "There was a problem sending the https request..."
    puts
  rescue Faraday::TimeoutError
    puts "Connection timeout error"
  end
  
 
end






def saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
     ussdObject = UssdRequest.new
     ussdObject.sequence = sequence
     ussdObject.service_key = service_key
     ussdObject.session_id = sessionID
     ussdObject.mobile_number = mobile_number
     ussdObject.ussd_body = ussd_body
     ussdObject.created_at = Time.now
     ussdObject.save
end




def saveTmpVals(sys_name,amount,session_val,return_code,prn,partial_val,mobile,session_id)
  TmpVal.create(
    sys_name: sys_name,
    amount: amount,
    session_val: session_val,
    return_code: return_code,
    prn: prn,
    partial_val: partial_val,
    created_at: Time.now,
    mobile: mobile,
    session_id: session_id
    )
end