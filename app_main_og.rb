  #require 'sinatra'
  #require 'active_record'
  #require 'json'
  #require 'net/http'
  #require 'savon'
  require './apis'
  #require 'faraday'

  
  STRRESPONSE = "RESPONSE"
  STRFALSE = "FALSE"
  STRTRUE = "TRUE"
  SEQUENCE = "SEQUENCE"
  STRMRCHNT = "233261064828"
  STRNCKNAME = "APPSNMOBIL"

  class UssdRequest < ActiveRecord::Base
   self.table_name = 'lkm_ussd_requests'
  end

  STRSCKEY = ""
  STRPUBKEY = ""



  post '/' do
       request.body.rewind
       @payload=JSON.parse(request.body.read)
       json_vals = @payload
       sequence=json_vals['SEQUENCE']
       end_session=json_vals['END_OF_SESSION']
       sessionID=json_vals['SESSION_ID']
       service_key=json_vals['SERVICE_KEY']
       mobile_number=json_vals['MOBILE_NUMBER']
       ussd_body=json_vals['USSD_BODY'].strip if json_vals.has_key?('USSD_BODY')
       saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
       process(sequence,service_key,sessionID,mobile_number,ussd_body,end_session)
  end


 post '/lekma_remote_resp' do
    


 end







  def process(sequence, service_key, sessionID, mobile, ussd_body, end_of_session='True')
    if sequence=="0"
        s_params = Hash.new
        s_params['SEQUENCE']=sequence
        s_params['REQUEST_TYPE']=STRRESPONSE
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY']="Pay bills via Tucksee Pay\n\n 1. Tucksee Pay \n 2. Other Payment Method"
        p s_params.to_json
    elsif sequence == "1"
    
        s_params = Hash.new
        s_params['SEQUENCE'] = sequence
        s_params['REQUEST_TYPE'] = STRRESPONSE
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY'] = ussd_body.strip
        if ussd_body == "1"
        s_params['END_OF_SESSION']=STRFALSE
        s_params['USSD_BODY']="Please Enter PRN: \n"
        else
        s_params['END_OF_SESSION']=STRTRUE
        s_params['USSD_BODY']="Other Payment Methods Not Available. Try again later"
      end
          p s_params.to_json
          
          
    elsif sequence == "2"
          s_params=Hash.new
          s_params['SEQUENCE'] = sequence
          s_params['REQUEST_TYPE'] = STRRESPONSE
          s_params['END_OF_SESSION'] = STRFALSE
          @payment_reference = "#{ussd_body}"
          auth_array = apiAuthenticate()
          @auth_session = auth_array[0]
          @auth_success = auth_array[1]
          if @auth_success == "1"
                verify_array_values = verifyReference(@auth_session,@payment_reference,mobile,sessionID)
                @verifySystemName = verify_array_values[0]
                @verify_payment_amount = verify_array_values[1]
                @verify_session = verify_array_values[2]
                @verify_success = verify_array_values[3]
                @verify_prn = verify_array_values[4]
                @verify_partial_value = verify_array_values[5]
                
                 if @verify_partial_value == "0"
                 s_params['USSD_BODY'] = "CONFIRM PAYMENT \n\n PRN: #{@payment_reference}  \n Account: #{@verifySystemName} \n Amount Due: #{@verify_payment_amount}  \n \n 1. Confirm \n 2. Cancel:"
                 s_params['END_OF_SESSION'] = STRFALSE
              
                 else
                   
                 s_params['END_OF_SESSION'] = STRFALSE
                 s_params['USSD_BODY'] = "CONFIRM PAYMENT \n\n PRN: #{@payment_reference}  \n Account: #{@verifySystemName} \n Amount Due: #{@verify_payment_amount}  \n \n Enter Amount:"
                 end
       
         else
               s_params['USSD_BODY'] = "Error In Connecting"
               s_params['END_OF_SESSION'] = STRTRUE
               p     s_params.to_json
         end 
          p     s_params.to_json
    elsif sequence == "3"
          s_params=Hash.new
          s_params['SEQUENCE']=sequence
          s_params['REQUEST_TYPE']=STRRESPONSE
          s_params['END_OF_SESSION']=STRFALSE
          if ussd_body == "1" #confirm[transact and close transaction]
                the_params = LKMVal.where("session_id=? and mobile_number=?", sessionID, mobile).order("id desc").limit(1)
                sessionKay = the_params[0]["verify_session_val"]
                verPayRef  = the_params[0]["verify_pay_ref"]
                vrSuccCode = the_params[0]["verify_succ_code"]
                verSysName = the_params[0]["verify_sys_name"]
                verAmount = the_params[0]["verify_amount"]
                verPartialValue = the_params[0]["ver_partial"]
                paidDate = Time.new
                
                  if  vrSuccCode == "1"
                   task = Thread.new{
                       transact_array_values = transact(sessionKay,verPayRef)
                       transaction_session = transact_array_values[0]
                       transaction_prn = transact_array_values[1]
                       transactionID = transact_array_values[3]
                       transaction_success = transact_array_values[2]
                          if transaction_success == "1" 
                              puts "supposedToCloseHere"
                                saveTransaction("Credit",paidDate,verAmount,transaction_prn,transactionID)
                                closeTransaction(transaction_session,transaction_prn,transactionID)
                               puts "supposedToSaveHere" 
                               puts "actuallySaved"
                             end
              }
                s_params['END_OF_SESSION']=STRTRUE
                s_params['USSD_BODY']="Your transaction is being processed."
                 task = Thread.new{
                      msgHash={ merchant_number: STRMRCHNT, mobile_number: mobile, amount: verAmount, reference: 'AirWallet', nickname: STRNCKNAME }
                      response=mobilePayment(msgHash)
                      puts response.inspect
            }
                else
                s_params['END_OF_SESSION'] = STRTRUE
                s_params['USSD_BODY'] = "Your transaction failed to process.Try again later.\n Thank you."
                p s_params.to_json
                end
          else
            s_params['END_OF_SESSION']=STRTRUE
            s_params['USSD_BODY']="Your transaction is being processed."
            
            #postParamsHere
            task = Thread.new{
                 msgHash={ merchant_number: STRMRCHNT, mobile_number: mobile, amount: verAmount, reference: 'AirWallet', nickname: STRNCKNAME }
                 response=mobilePayment(msgHash)
                 puts response.inspect
            }
         end
           p s_params.to_json
    elsif sequence == "4"
          s_params=Hash.new
          s_params['SEQUENCE']=sequence
          s_params['REQUEST_TYPE']=STRRESPONSE
          s_params['END_OF_SESSION']=STRFALSE
          s_params['USSD_BODY'] = ussd_body.strip
       if ussd_body == "1"
         #call transaction and close
         the_params = LKMVal.where("session_id=? and mobile_number=?", sessionID, mobile).order("id desc").limit(1)
         sessionKay = the_params[0]["verify_session_val"]
         verPayRef  = the_params[0]["verify_pay_ref"]
         vrSuccCode = the_params[0]["verify_succ_code"]
         verSysName = the_params[0]["verify_sys_name"]
         verAmount = the_params[0]["verify_amount"]
         
         if  vrSuccCode == "1"
           task = Thread.new{
             transact_array_values = transact(sessionKay,verPayRef)
             transaction_session = transact_array_values[0]
             transaction_prn = transact_array_values[1]
             transactionID = transact_array_values[3]
             transaction_success = transact_array_values[2]
              if transaction_success == "1" 
              closeTransaction(transaction_session,transaction_prn,transactionID)
              end
              }
              s_params['END_OF_SESSION']=STRTRUE
              s_params['USSD_BODY']="Your transaction will be processed."
              p s_params.to_json
          else
            s_params['END_OF_SESSION']=STRTRUE
            s_params['USSD_BODY']="Function Error"
            p s_params.to_json
         end
        else
         s_params['END_OF_SESSION']=STRTRUE
         s_params['USSD_BODY']="Your transaction has been cancelled."
        end
        p s_params.to_json
    end
  end


  def saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
     ussdObject = UssdRequest.new
     ussdObject.sequence = sequence
     ussdObject.service_key = service_key
     ussdObject.session_id = sessionID
     ussdObject.mobile_number = mobile_number
     ussdObject.ussd_body = ussd_body
     ussdObject.created_at = Time.now
     ussdObject.save
  end
  
  
  
 def mobilePayment(msgHash)
   client = Savon.client(wsdl: 'http://41.190.91.198:10088/wservices.php?wsdl')
   response = client.call(:purchase_info, message: msgHash)
   resp=""
   resp=response.body[:purchase_info_response][:response]
   respArr=resp.split("~")
 end
  
  
def postParams()
  
end