#require 'savon'
#require 'nokogiri'
#require 'csv' #eod
#require 'base64' #for EOD purpose
#require 'active_record' #for EOD purpose
#require 'digest/md5' #for EOD purpose
#require 'json' #for EOD purpose
#require 'net/http' #for EOD purpose
#require 'faraday' #for EOD purpose

ActiveRecord::Base.establish_connection(
    :adapter  => "mysql2",
    :host     => "localhost",
    :username => "jude",
    :password => "$jude1",
    :database => "jd_db"
  )




STRUID = "UIDAIRTELACCOUNT"
STRAPIKEY = "EBF6A274495FE8A465480E5359E935E2"
STRPASS = 'WhN!$#$uPNiSzvrpvH1q5LzfrRe-8$KK'
STRKEYMD5 = "90B2B45AD908A40F52C7DC8CCC0CB644"
STRCLIENT = Savon.client(wsdl: 'https://api.tpaygo.com/wsdl/PaymentNotificationService/?wsdl')



class LKMTransaction < ActiveRecord::Base
  self.table_name = 'lkm_transactions'
end


class LKMVal < ActiveRecord::Base
  self.table_name = 'lkm_verify_vals'
end


class Client < ActiveRecord::Base
 self.table_name = 'lkm_clients'
end







def apiAuthenticate
 

  hash_string = STRAPIKEY + STRUID + STRPASS + STRKEYMD5
  hash_string = hash_string
  hash_val = Digest::MD5.hexdigest(hash_string)
  msgHash = Hash.new
  
  

  msgHash={'api_key' => STRAPIKEY ,'api_username' => STRUID, 'api_password' => STRPASS, 'hash' => hash_val, 'backref' => ""}
  #puts msgHash.inspect
  response = STRCLIENT.call(:authenticate, message: msgHash)
  puts "#########Authenticate Response#########"
  puts response
  #puts "#######################################"
  resp=response.body[:authenticate_response][:return]
  @doc = Nokogiri::XML(resp)
  success_code = ""
  key = ""
   
   @doc.xpath("//success").each do |root|
    success_code = root.text
    #puts "*****"
    #puts "Message: " + success_code
    #puts "*****"
   end
   
   
   if success_code == "1"
     @doc.xpath("//sessionKey").each do |root|
      key = root.text
      #puts "********************"
      #puts "Authenticate Session Key: " + key
      #puts "*******************"
     end
   else
     puts "Error In Connecting......."
   end
   
   
   _auth_hash = {"key" => key, "success_code" => success_code }
   
   return _auth_hash
end




def verifyReference(session_key,payment_reference,mobile_number,ussd_session)
  
  hash = ''
  backref = ''
  hash_string = session_key + payment_reference + STRKEYMD5
  hash_val = Digest::MD5.hexdigest(hash_string)
  
  
  #puts "#####Verify Ref. Hash Value######"
  #puts hash_val
  #puts "#################################"
  
  #puts "#####Session Key From Auth.######"
  #puts session_key
  #puts "#################################"
  
  
  
  msgHash = Hash.new
  msgHash={'session_key'=> session_key, 'reference' => payment_reference, 'hash'=>hash_val, 'backref' =>""}
  response = STRCLIENT.call(:verify_reference, message: msgHash)
  puts
  puts
  resp=response.body[:verify_reference_response][:return]
  
  @xml_doc = Nokogiri::XML(resp)
  
  #puts "&&&&&&&"
  #puts resp
  #puts "&&&&&&&"
  
  successCode = ""
  sesskey = ""
  prn = ""
  system_name = ""
  payment_amount = ""
  allow_partial = ""
  tpgo_ref = ""
  expiry_date = ""
  status = ""
  
  @xml_doc.xpath("//success").each do |root|
   successCode = root.text
   #puts "*****"
   #puts "Message Code: " + successCode
   #puts "*****"
   end
  
  if successCode == "1"
   @xml_doc.xpath("//prn").each do |root|
    prn = root.text
    #puts "*****"
    #puts "Payment Reference Number: " + prn
    #puts "*****"
    end
    
    @xml_doc.xpath("//tpgoReference").each do |root|
    tpgo_ref = root.text
    #puts "*****"
    #puts "tpgoReference " + tpgo_ref
    #puts "*****"
    end
    
    @xml_doc.xpath("//status").each do |root|
    status = root.text
    #puts "*****"
    #puts "Status" + status
    #puts "*****"
    end
    
    @xml_doc.xpath("//expiryDate").each do |root|
    expiry_date = root.text
    #puts "*****"
    #puts "Expiry Date " + expiry_date
    #puts "*****"
    end
    
    @xml_doc.xpath("//sessionKey").each do |root|
     sesskey = root.text
      #puts "*****"
      #puts " Verify Session Key: " + sesskey
      #puts "*****"
     end
     @xml_doc.xpath("//systemName").each do |root|
      system_name = root.text
      #puts "*****System Name Here"
      #puts " System Name: " + system_name
      #puts "*****"
     end
      @xml_doc.xpath("//paymentAmount").each do |root|
      payment_amount = root.text
      #puts "*****"
      #puts " Payment Amount: " + payment_amount
      #puts "*****"
     end
      @xml_doc.xpath("//allowPartial").each do |root|
      allow_partial = root.text
      #puts "*****"
      #puts " Allow Partial : " + allow_partial
      #puts "*****"
     end
     
  Thread.new{
    verifyObj = LKMVal.new
    verifyObj.verify_session_val = sesskey
    verifyObj.verify_pay_ref = prn
    verifyObj.verify_succ_code = successCode
    verifyObj.verify_sys_name = system_name
    verifyObj.verify_amount = payment_amount
    verifyObj.mobile_number = mobile_number
    verifyObj.session_id = ussd_session
    verifyObj.ver_partial = allow_partial
    verifyObj.save
  }

     
  else
    puts "Function Error......"
 end

  
  verifyHash = {
                "system_name" => system_name, 
                "payment_amount" => payment_amount, 
                "sesskey" => sesskey, 
                "successCode" => successCode, 
                "prn" => prn, 
                "allow_partial" => allow_partial,
                "tpgo_ref" => tpgo_ref,
                "expiry_date" => expiry_date,
                "status" => status
                }
 
 return verifyHash
 #return system_name,payment_amount,sesskey,successCode,prn,allow_partial,tpgo_ref,expiry_date,status
 
end


def transact(session_key,payment_reference,transid,_pay_amt,tpgo_ref)
  #client = Savon.client(wsdl: 'https://tpg.tamsonline.co.za/api/wsdl/PaymentNotificationService/?wsdl')
  #secret_key = '4483015B1A99E7C63B7B1615BCF66C59'
  hash_string = session_key + payment_reference + STRKEYMD5
  hash_val = Digest::MD5.hexdigest(hash_string)
  puts
  puts
  puts
  puts "########Session Key From Verification######"
  puts session_key
  puts "##########################################"

  puts "####Transaction Hash Value######"
  puts hash_val
  puts "#################################"
  trans_id = transid
  prn = payment_reference
  transID = trans_id
  payment_method = "Credit"
  payment_date = Time.new.strftime("%d/%m/%Y")
  #value_date = ""
 
  puts
  puts
  puts '--------Transaction Date-----------'
  puts payment_date
  puts
  puts
 
  xml_string = "<?xml version='1.0'?><transactionRecord><prn>#{payment_reference}</prn><tpgoReference>#{tpgo_ref}</tpgoReference><transactionReference>#{trans_id}</transactionReference><amountPaid>#{_pay_amt}</amountPaid><paymentMode>1</paymentMode><status>C</status><paymentDate>#{payment_date}</paymentDate><valueDate>#{payment_date}</valueDate><chequeNumber></chequeNumber></transactionRecord>"
  msgHash={'session_key' => session_key, 'reference' => payment_reference, 'transaction' => xml_string ,'hash' => hash_val, 'backref' => ""}
  response = STRCLIENT.call(:transact, message: msgHash)
  puts response
  
  resp=response.body[:transact_response][:return]
  
  @xml_transact = Nokogiri::XML(resp)

  puts "Transaction Response"
  puts "*********************"
  puts resp
  puts "*********************"

  successCode = ""
  sessionKeyVal = ""
  payment_ref = payment_reference
  transaction_id = ""
  logout = ""
  hash = ""
  back_ref = ""
  tracking_id = ""
  
 @xml_transact.xpath("//success").each do |root|
 successCode = root.text
 puts "*****"
 puts "Success[1]/Failure[0]: " + successCode
 puts "*****"
 end
 
 if successCode == "1"
   @xml_transact.xpath("//sessionKey").each do |root|
   sessionKeyVal = root.text
   puts "************************************"
   puts "Transaction Session Key: " + sessionKeyVal
    puts "************************************"
    end
    @xml_transact.xpath("//trackingRef").each do |root|
   tracking_id = root.text
   puts "************************************"
   puts "Transaction Session Key: " + sessionKeyVal
    puts "************************************"
    end
    #saveTransactionBeforeClose
    #saveTransactionHere
  puts
  puts
  puts "***Saving Transaction Into DB***"
  #saveTransaction(payment_method,payment_date,amount_paid,prn,transID)
  puts
  puts "*********************************"
  #closeTransaction(sessionKeyVal,payment_reference,trans_id)
  else
   puts "Function Error......"
 end



 _transact_hash = {
   "sessionKeyVal" => sessionKeyVal,
   "payment_ref" => payment_ref,
   "successCode" => successCode,
   "trans_id" => trans_id,
   "payment_method" => payment_method,
   "payment_date" => payment_date,
   "tracking_id" => tracking_id 
 }





 #return sessionKeyVal,payment_ref,successCode,trans_id,payment_method,payment_date,tracking_id
 
 return _transact_hash
 
end





def closeTransaction(session_key,payment_reference,transaction_id)
   puts "<--------Closing Transaction-------->"
   #secret_key = "4483015B1A99E7C63B7B1615BCF66C59"
   puts
   puts "###SESSION KEY FROM TRANSACTION#####"
   puts session_key
   puts "##################################"
   
   hash_string = session_key + payment_reference + transaction_id + STRKEYMD5
   hash_val = Digest::MD5.hexdigest(hash_string)
   puts
   puts
   puts "#####Closing Transaction Hash Value####"
   puts hash_val
   puts "#######################################"
   #client = Savon.client(wsdl: 'https://tpg.tamsonline.co.za/api/wsdl/PaymentNotificationService/?wsdl')
   
   puts
   puts "**Payment Reference**"
   puts payment_reference
   puts "********************"
   
   msgHash = {'session_key' => session_key, 'reference' => payment_reference, 'transaction_id' => transaction_id, 'logout' => 1 ,'hash' => hash_val, 'backref' => ""}
  
   resp = STRCLIENT.call(:close_transaction, message: msgHash) 
  
   _response = resp.body[:close_transaction_response][:return]
   
   @xml_close = Nokogiri::XML(_response)

   puts "####Close Transaction Response#####"
   puts _response
   puts "###################################"
   
    successCode = ""
    sessionKeyVal = ""
    payment_ref= payment_reference
    transaction_id = ""
    logout = ""
    hash = ""
    back_ref = ""
    strmsg = ""
   @xml_close.xpath("//success").each do |root|
      successCode = root.text
      puts "*****"
      puts "Success[1]/Failure[0]: " + successCode
      puts "*****"
 end
end












def genUniqueCodeCredit
    #day= Time.new.strftime("%d")
    #month = Time.new.strftime("%m")
    #year = Time.new.strftime("%y")
    #hour = Time.new.strftime("%H")
    #minute = Time.new.strftime("%M")
    #micro = Time.new.strftime("%L")
    #first_two = rand(00..99)
    #first_two = first_two.to_s.rjust(2, "0")
    last_two  = rand(00..99)
    last_two = last_two.to_s.rjust(2, "0")
    #uniq_id = "CR#{year}#{first_two}#{month}#{last_two}#{day}#{hour}#{minute}"
    #puts ""
    #puts
    #puts
    
    id = Time.new.strftime("%Y%m%d%H%M%S%L")
    
    
   _new_id = loop do
      token = id.to_i.to_s(36)
      break token unless Dependentval.exists?(trans_id: token)
    end
    
    
    unique_id = "CR#{last_two}#{_new_id}"
    
    
    
    return unique_id
end





#send EOD at the end of every working day
#authenticate before you send EOD Report
#content CSV base64{Rocket Science;Just Kidding}

def EODReport(session_key,client_id,content)
 _arr = Client.where(id: client_id)[0]
 hash_string = session_key + _arr.tpgo_ref + STRKEYMD5
 hash_value = Digest::MD5.hexdigest(hash_string)
 msgHash={'session_key' => session_key, 'tpgo_reference' => _arr.tpgo_ref, 'content' => content, 'logout' => 1 ,'hash' => hash_value, 'backref' => ""}
 response = STRCLIENT.call(:eod_report, message: msgHash)
 puts
 puts
 puts "####EOD Report Response#####"
 puts response
 puts "############################"
end








def saveTransaction(paid_date,amount,prn,transaction_id,client_id,tracking_id)
  transObj = LKMTransaction.new
  transObj.trans_type = "Credit"
  transObj.paid_date = paid_date
  transObj.amount = amount
  transObj.prn = prn
  transObj.trans_ref = transaction_id
  transObj.client_id = client_id
  transObj.tracking_id = tracking_id
  transObj.save
end















def mainMethod
 payment_reference = "LKPBR17000000574"
 auth_array = apiAuthenticate()
 auth_session = auth_array[0]
 auth_success = auth_array[1]

if auth_success == "1"
  verify_array_values = verifyReference(auth_session,payment_reference)
  verifySystemName = verify_array_values[0]
  verify_payment_amount = verify_array_values[1]
  verify_session = verify_array_values[2]
  verify_success = verify_array_values[3]
  verify_prn = verify_array_values[4]
    if  verify_success == "1"
        transact_array_values = transact(verify_session,verify_prn)
        transaction_session = transact_array_values[0]
        transaction_prn = transact_array_values[1]
        transactionID = transact_array_values[3]
        transaction_success = transact_array_values[2]
        closeTransaction(transaction_session,transaction_prn,transactionID)
     else
     puts "System Error...."
     end
else
     puts "System Error........."
  end
end


#trans_type | paid_date  | amount | prn  | trans_ref 
#type
#paid_date
#Base64.encode(string)

def export_to_csv
    @transactions = LKMTransaction.where(:client_id => 1)
    csv_string = CSV.generate do |csv|
         @transactions.each do |transaction|
           amount = sprintf('%.2f', transaction.amount)
           
           csv << [transaction.trans_type, transaction.paid_date.strftime("%m/%d/%Y") , amount, transaction.prn, transaction.trans_ref]
           
           
         end
    end         
end




#_authenticate = apiAuthenticate
#_sessionkey = _authenticate[0]
#client_id = 1




#_csv = export_to_csv
#_content = Base64.encode64(_csv)
#EODReport(_sessionkey,client_id,_content)