require 'sinatra'
require 'savon'
require 'active_record'
require 'json'
require 'net/http'
require 'faraday'
require 'digest/md5'
require 'openssl'
require 'nokogiri'
require './apis'
require 'csv'



ActiveRecord::Base.establish_connection(
    :adapter  => "mysql2",
    :host     => "localhost",
    :username => "jude",
    :password => "$jude1",
    :database => "jd_db"
  )





STRXPIRED = "X"
STRCANC = "C"
STRINVALD = "N"
STRTRNSCTD = "T"
 STRAVAIL = "A"
STRSENDER = "Tucksee"


STROPENTIMEOUT ="180"
STRRESPONSE = "RESPONSE"
STRFALSE = "FALSE"
STRTRUE = "TRUE"
SEQUENCE = "SEQUENCE"
STRMRCHNT = "233269058733"
STRNCKNAME = "TUCKSEE"
STRSECRKEY = "I7QWq2CVzddk1aZKKRw6uB5Qg7u1X/uY8VCOsKxOt+T8pXY6tLnR5GhzszUSz/TC1m4LoLQj1RyLn9I4IBMFsQ=="
STRPUBKEY = "+P7q7paQ4DnWfWtluZ5R+bdljlA779feZVr4ZeE5Er4fEo5NAX0g6xIgHSFR2bCHWL83SXT0WH0hq+ERRF1LOw=="
STRINVPRN = "Dear customer, the provided reference number is invalid."

STRREF = "LEKMA"
URL='http://184.173.139.74:8198'
HEADER={'Content-Type'=>'Application/json','timeout'=>'180'}
MM_URL   =  'https://appsnmobileagent.com:8201'
MM_HEADERS = {'Content-Type'=> 'application/json','timeout'=>STROPENTIMEOUT, 'open_timeout'=>STROPENTIMEOUT}





SMS_CONN = Faraday.new(:url => URL,:headers => HEADER) do |f|
   f.response :logger
   f.adapter Faraday.default_adapter
end
NEW_CONN = Faraday.new(:url => MM_URL, :headers => MM_HEADERS, :ssl => {:verify => false}) do |faraday|   
  faraday.request  :url_encoded             # form-encode POST params
  faraday.response :logger                  # log requests to STDOUT
  faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
end





class USSDRequest < ActiveRecord::Base
 self.table_name = 'lkm_ussd_requests'
end


class LKMTransaction < ActiveRecord::Base
  self.table_name = 'lkm_transactions'
end


class LKMVal < ActiveRecord::Base
  self.table_name = 'lkm_verify_vals'
end


class Dependentval < ActiveRecord::Base
	self.table_name = 'dependent_vals'
end



class MmLog < ActiveRecord::Base
  self.table_name = 'tucksee_mm_logs'
end



class SMS < ActiveRecord::Base
  self.table_name = 'tucksee_msgs'
end


post '/testingServer' do
	request.body.rewind
	req = JSON.parse(request.body.read)
  resp_code=req["trans_status"]
end





post '/lekma_remote_response' do
  request.body.rewind
  req = JSON.parse(request.body.read)
  resp_code=req["trans_status"]
  resp_desc=req["message"]
  refID = req["trans_ref"]
  am_refID = req["trans_id"]
  
  #puts
  #puts
  #puts "------------------CallBack---------------"
  #puts "###############################################"
  #puts
  
  _dep_arr = Dependentval.where(trans_id: refID).order("id desc").limit(1)[0]
  
  #puts
  #puts
  #puts "-----------Payment Amount--------"
  #puts
  #puts _dep_arr.payment_amount
  
  #puts _dep_arr.inspect
  #puts
  #puts
  
  
  #__task = Thread.new {
  if resp_code == '200' then 
    #dotransaction and close transaction
    #task = Thread.new{
    if _dep_arr.partial_code == "0" then
           saveMobileMoney(resp_code,resp_desc,refID,am_refID) 
          
           #puts
           #puts
           #puts "################# Mobile Money Payment Was Successful ##################"
           #puts 
           #puts
           transact_array_values = transact(_dep_arr.session_val,_dep_arr.prn,refID,_dep_arr.payment_amount,_dep_arr.tpgo_ref)
           transaction_session = transact_array_values["sessionKeyVal"]
           transaction_prn = transact_array_values["payment_ref"]
           transactionID = transact_array_values["trans_id"]
           transaction_success = transact_array_values["successCode"]
           trans_paid_date = transact_array_values["payment_date"]
           _tracking_id = transact_array_values["tracking_id"]
           
          _client = Client.where(tpgo_ref: _dep_arr.tpgo_ref)[0] 
          _client_id = _client.id
          
          
          _curr = _client.currency_code
          _amt_fmormat = '%.2f' % _dep_arr.payment_amount.to_f
          _str_msg = "Reference No.: #{transaction_prn} Account: #{_dep_arr.sys_name} Amount: #{_curr} #{_amt_fmormat} Payment Date: #{trans_paid_date} Transaction Reference: #{transactionID}"
                   
          
          
           if transaction_success == "1" 
            
            saveTransaction(trans_paid_date,_dep_arr.payment_amount,_tracking_id,transactionID,_client_id,_tracking_id)
          
            task= Thread.new{
                saveSMS(STRSENDER,_dep_arr.mobile,_str_msg)
                sendMessage(STRSENDER,_dep_arr.mobile,_str_msg)
            }
            
            closeTransaction(transaction_session,transaction_prn,transactionID)
      
           elsif transaction_success == '0'
            puts
            puts "##########Transaction Wasn't Successful and DID Not Close##############"
           end
      
    else
            saveMobileMoney(resp_code,resp_desc,refID,am_refID) 
    
     #puts
     #puts
     #puts "################# Mobile Money Payment Was Successful ##################"
     #puts 
     #puts
     transact_array_values = transact(_dep_arr.session_val,_dep_arr.prn,refID,_dep_arr.partial_amt,_dep_arr.tpgo_ref)
     transaction_session = transact_array_values["sessionKeyVal"]
     transaction_prn = transact_array_values["payment_ref"]
     transactionID = transact_array_values["trans_id"]
     transaction_success = transact_array_values["successCode"]
     trans_paid_date = transact_array_values["payment_date"]
     _tracking_id = transact_array_values["tracking_id"]
     
    _client = Client.where(tpgo_ref: _dep_arr.tpgo_ref)[0] 
    _client_id = _client.id
    
    
    _curr = _client.currency_code
    _amt_fmormat = '%.2f' % _dep_arr.partial_amt.to_f
    _str_msg = "Reference No.: #{transaction_prn} Account: #{_dep_arr.sys_name} Amount: #{_curr} #{_amt_fmormat} Payment Date: #{trans_paid_date} Transaction Reference: #{transactionID}"
             
    
    
     if transaction_success == "1" 
      
      saveTransaction(trans_paid_date,_dep_arr.partial_amt,_tracking_id,transactionID,_client_id,_tracking_id)
    
      task= Thread.new{
          saveSMS(STRSENDER,_dep_arr.mobile,_str_msg)
          sendMessage(STRSENDER,_dep_arr.mobile,_str_msg)
      }
      
      closeTransaction(transaction_session,transaction_prn,transactionID)

     elsif transaction_success == '0'
      puts
      puts "##########Transaction Wasn't Successful and DID Not Close##############"
     end
      
    end

    
    
   
              
    
  else
    #payment failed do nothing...send notification to user
    saveMobileMoney(resp_code,resp_desc,refID,am_refID)
    puts
    puts
    puts "-----------------------Payment Failure--------------------"
    puts "Your transaction Failed. PLease try again later..........."
    puts
  end
  #}
  

end





post '/' do
		 request.body.rewind
		 @payload=JSON.parse(request.body.read)
		 json_vals = @payload
	 	 sequence=json_vals['SEQUENCE']
	   end_session=json_vals['END_OF_SESSION']
	   sessionID=json_vals['SESSION_ID']
	   service_key=json_vals['SERVICE_KEY']
	   mobile_number=json_vals['MOBILE_NUMBER']
	   ussd_body=json_vals['USSD_BODY'].strip if json_vals.has_key?('USSD_BODY')
	   saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
	   process(sequence,service_key,sessionID,mobile_number,ussd_body,end_session)
end



def process(sequence, service_key, sessionID, mobile, ussd_body, end_of_session='True')
  if sequence=="0"
    s_params = Hash.new
    s_params['SEQUENCE']=sequence
    s_params['REQUEST_TYPE']=STRRESPONSE
    s_params['END_OF_SESSION']=STRFALSE
    s_params['USSD_BODY']= "Pay bills via Tucksee Pay \n\n 1. Tucksee Pay \n"
    p s_params.to_json
  elsif sequence == "1"
    s_params = Hash.new
    s_params['SEQUENCE'] = sequence
    s_params['REQUEST_TYPE'] = STRRESPONSE
    s_params['END_OF_SESSION']=STRFALSE
    s_params['USSD_BODY'] = ussd_body.strip
    
    if ussd_body == "1"
      s_params['END_OF_SESSION']=STRFALSE
      s_params['USSD_BODY']="\nTucksee Pay \n\n Please Enter Reference No.:"
   # elsif ussd_body == "2"
     # s_params['END_OF_SESSION']=STRTRUE
     # s_params['USSD_BODY']="\nOther payments method not available.\n\n Please try again later."
    else
      s_params['END_OF_SESSION']=STRTRUE
      s_params['USSD_BODY']="\nYou chose a wrong option.\n\n Dial *456*15# and choose an option."
    end
    p s_params.to_json
  elsif sequence == "2"
    strbody = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '1').pluck(:ussd_body)
    _prn = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '2').pluck(:ussd_body)[0]
  
    s_params = Hash.new
    s_params['SEQUENCE'] = sequence
    s_params['REQUEST_TYPE'] = STRRESPONSE
    s_params['END_OF_SESSION']=STRFALSE
    s_params['USSD_BODY'] = ussd_body.strip
    
    if strbody[0].strip=='1'
    	#authenticate here and return values
     _prn = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '2').pluck(:ussd_body)[0]

    	 auth_array = apiAuthenticate()
    	_auth_session = auth_array["key"]
      _auth_success = auth_array["success_code"]
    	
    	if _auth_success == '1'
    	  
    	  #Thread.new{
    	    
    	 
    		 verify_array_values = verifyReference(_auth_session,_prn,mobile,sessionID)
    		 @verifySystemName = verify_array_values["system_name"]
         @verify_payment_amount = verify_array_values["payment_amount"]
         @verify_session = verify_array_values["sesskey"]
         @verify_success = verify_array_values["successCode"]
         @verify_prn = verify_array_values["prn"]
         @verify_partial_value = verify_array_values["allow_partial"]
         @tpgo_ref = verify_array_values["tpgo_ref"]
         @expiry_date =  verify_array_values["expiry_date"]
         @status =  verify_array_values["status"]
         @pay_type = ""
         
    		 # }
    		 _curr_code = Client.where(tpgo_ref: @tpgo_ref).pluck(:currency_code)[0]
    	   

    		 
    	if @status ==  STRXPIRED then
    			s_params['USSD_BODY'] = "\nTucksee Pay\n Reference number is expired."
          s_params['END_OF_SESSION'] = STRTRUE
    			
    	elsif @status == STRCANC then
    		  s_params['USSD_BODY'] = "\nTucksee Pay\n Reference number has been cancelled."
          s_params['END_OF_SESSION'] = STRTRUE
    		
    	elsif @status == STRINVALD then
    			s_params['USSD_BODY'] = "\nTucksee Pay\n #{STRINVPRN}"
          s_params['END_OF_SESSION'] = STRTRUE
    		
    	elsif @status == STRTRNSCTD then
    			s_params['USSD_BODY'] = "\nTucksee Pay.\n PRN Transaction already completed."
          s_params['END_OF_SESSION'] = STRTRUE
    	elsif @verify_success == '1' && @status == STRAVAIL
    			if @verify_partial_value == '0'
    			  Thread.new{
    			     _transaction_id = genUniqueCodeCredit
    			      saveDependentVals(@verifySystemName,@verify_payment_amount.to_d.truncate(2).to_f ,mobile,@verify_session,@verify_success,@verify_prn,@verify_partial_value,_transaction_id,sessionID,@tpgo_ref)

    			 }
			       		s_params['END_OF_SESSION']=STRFALSE
			       		s_params['USSD_BODY']="\nTucksee Pay \n Confirm Amount \n\nReference No. : #{_prn}  \nAccount: #{@verifySystemName} \nAmount: #{_curr_code} #{@verify_payment_amount}  \n\nEnter Amount:"
       	 elsif @verify_partial_value == '1'
       			   	#allow partial
       			   	Thread.new{
       			   	 _transaction_id = genUniqueCodeCredit
       			   	 saveDependentVals(@verifySystemName,@verify_payment_amount.to_d.truncate(2).to_f ,mobile,@verify_session,@verify_success,@verify_prn,@verify_partial_value,_transaction_id,sessionID,@tpgo_ref)
       			   	}
       					s_params['END_OF_SESSION']=STRFALSE
			       		s_params['USSD_BODY']="\nTucksee Pay \n Confirm Amount \n\nReference No.: #{_prn}  \nAccount: #{@verifySystemName} \nAmount: #{_curr_code} #{@verify_payment_amount}  \n\nEnter Amount:"
       	  end
       	  
      elsif @verify_success == '0' then
       	s_params['USSD_BODY'] = "\nTucksee Pay\n\nThere was a problem verifying your reference number please try again."
        s_params['END_OF_SESSION'] = STRTRUE
       
    	end
    		
   		elsif _auth_success == '0'
   				s_params['USSD_BODY'] = "\nTucksee Pay\n\nThere was a problem in completing your transaction. Please try again later."
          s_params['END_OF_SESSION'] = STRTRUE
   		end	
   		#p     s_params.to_json
   		
    elsif strbody[0].strip=='2'
    
    
    
    
    elsif strbody[0].strip=='3'
      
      
      
      
      
    elsif strbody[0].strip=='4'
     
    #else
     #s_params['END_OF_SESSION']=STRTRUE
     #s_params['USSD_BODY']="\nYou selected a wrong option.\n\n Dial *456*15# and select an option."
    end
    p  s_params.to_json
    
   
  elsif sequence == "3"
    strbody = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '1').pluck(:ussd_body)
    #_val = 
    s_params = Hash.new
    s_params['SEQUENCE'] = sequence
    s_params['REQUEST_TYPE'] = STRRESPONSE
    s_params['END_OF_SESSION']=STRFALSE
    s_params['USSD_BODY'] = ussd_body.strip
    
		_amount = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '3').pluck(:ussd_body)[0]
		
    if strbody[0].strip=='1'
    		#a.is_a? Integer
     		#if ussd_body.is_a? Integer
     				#_amount = "%gx" % (_amount.to_f / 100.00)
     			_amount = '%.2f' % _amount.to_f
     			#make payment request
     			_dep_arr = Dependentval.where(mobile: mobile,session_id: sessionID).order("id desc").limit(1)[0]
     			
     			
     			puts
     			puts
     			puts "-------Values------"
     			puts _dep_arr.inspect
     			puts
     		
     			_partial_val = _dep_arr.partial_code
     			_currency_code = Client.where(tpgo_ref: _dep_arr.tpgo_ref).pluck(:currency_code)[0]
     			
     		if _partial_val == '0'
     			#do Not Allow Partial
     				if _amount.to_f > _dep_arr.payment_amount.to_f || _amount.to_f < _dep_arr.payment_amount.to_f
     					s_params['END_OF_SESSION']=STRTRUE
         			s_params['USSD_BODY']="\nTucksee Pay \nYou must pay the amount in full."	
         		else
         			s_params['END_OF_SESSION']=STRFALSE
     			 	  s_params['USSD_BODY']="\nTucksee Pay \nConfirm Payment \n\nReference No.: #{_dep_arr.prn}  \nAccount: #{_dep_arr.sys_name} \nAmount To Pay: #{_currency_code} #{_amount}  \n\n1. Confirm\n2. Cancel"	
         		end
         		
         else
         		s_params['END_OF_SESSION']=STRFALSE
     			 	s_params['USSD_BODY']="\nTucksee Pay \n Confirm Payment \n\nReference No.: #{_dep_arr.prn}  \nAccount: #{_dep_arr.sys_name} \nAmount To Pay: #{_currency_code} #{_amount}  \n\n1. Confirm\n2. Cancel"	

         	
     		end
     			
 					
     	
    elsif strbody[0].strip=='2'
     
     
     
    elsif strbody[0].strip=='3'
    
    
    
     
    elsif strbody[0].strip=='4'
    
    end
    p  s_params.to_json
    
  elsif sequence == "4"
    strbody = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '1').pluck(:ussd_body)
    s_params=Hash.new
    s_params['SEQUENCE']=sequence
    s_params['REQUEST_TYPE']=STRRESPONSE
    s_params['END_OF_SESSION']=STRFALSE
    s_params['USSD_BODY'] = ussd_body.strip
    
    _amount = USSDRequest.where("service_key=? and session_id=? and mobile_number=? and sequence=?", service_key,sessionID, mobile, '3').pluck(:ussd_body)[0]
    
    
    
    
    if strbody[0].strip=='1'
    	
    	if ussd_body == '1'
    	
    		_amount_float = '%.2f' % _amount.to_f
     		_update = Dependentval.where(mobile: mobile, session_id: sessionID).order("id desc").limit(1).update_all(partial_amt: _amount_float)
     		
     		_dep_arr = Dependentval.where(mobile: mobile,session_id: sessionID).order("id desc").limit(1)[0]

     		if _update then
     				_task = Thread.new{
     				mobileMoneyPayment(STRMRCHNT,_dep_arr.sys_name,_dep_arr.prn,_dep_arr.mobile,_amount_float,_dep_arr.trans_id,STRPUBKEY,STRSECRKEY)
     			}
     			
     			s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="\nTucksee Pay \nYour transaction is being processed.Please wait to authorize and complete transaction."
     		else
     			s_params['END_OF_SESSION']=STRTRUE
          s_params['USSD_BODY']="\nTucksee Pay \nThere was a problem processing your transaction. Please try again later."
     		end
     	else
     		s_params['END_OF_SESSION']=STRTRUE
        s_params['USSD_BODY']="\nTucksee Pay \nYou cancelled your transaction."
     
			end	
     	
    else
      s_params['END_OF_SESSION']=STRTRUE
      s_params['USSD_BODY']="\n System is currently unavailable.Please try again later"
    end
    p  s_params.to_json
  end
end






def saveSession(sequence, service_key, sessionID, mobile_number, ussd_body)
	 ussdObject = USSDRequest.new
   ussdObject.sequence = sequence
   ussdObject.service_key = service_key
   ussdObject.session_id = sessionID
   ussdObject.mobile_number = mobile_number
   ussdObject.ussd_body = ussd_body
   ussdObject.created_at = Time.now
   ussdObject.save
end



def mobileMoneyPayment(merchant_no,nick_name,reference,mobile_number,amount,trnx_id,client_id,secret_key)

  endpoint = "/debitCustomerWallet"
  
  ts=Time.now.strftime("%Y-%m-%d %H:%M:%S")

  payload={ 
      :merchant_number=> merchant_no,
      :customer_number=> mobile_number,
      :amount=> amount,
      :reference=> reference,
      :exttrid=> trnx_id,
      :nickname=>nick_name,
      :ts=>ts
      }

  json_payload=JSON.generate(payload)
  msg_endpoint="#{endpoint}#{json_payload}"
  
  puts
  puts msg_endpoint
  puts


  def computeSignature(secret, data)
      digest=OpenSSL::Digest.new('sha256')
      signature = OpenSSL::HMAC.hexdigest(digest, secret.to_s, data)
      return signature
  end
 

  signature=computeSignature(secret_key, msg_endpoint)
  
  begin
  res=NEW_CONN.post do |req|
    req.url endpoint
    req.options.timeout = 30           # open/read timeout in seconds
    req.options.open_timeout = 30      # connection open timeout in seconds
    req["Authorization"]="#{client_id}:#{signature}"
    req.body = json_payload
  end
  
  
  
  rescue Faraday::SSLError
    puts
    puts "There was a problem sending the https request..."
    puts
  rescue Faraday::TimeoutError
    puts "Connection timeout error"
  end
  
 
end


def saveDependentVals(sys_name,payment_amt,mobile,session_val,return_code,prn,partial,trans_id,session_id,tpgo_ref)
	Dependentval.create(
		sys_name: sys_name,
		payment_amount: payment_amt,
		mobile: mobile,
		session_val: session_val,
		return_code: return_code,
		prn: prn,
		partial_code: partial,
		trans_id: trans_id,
		created_at: Time.now,
		session_id: session_id,
		tpgo_ref: tpgo_ref
		)
end

#partial_amt



def sendMessage(sender,mobile_number,msg)
      sms_params={
          "sender":sender.to_s,
          "mobile_number":mobile_number.to_s, 
          "msg":msg.to_s, 
          "msg_id":"100"
         }
      sms_load=sms_params.to_json
      resp_check=SMS_CONN.post do |t|
                   t.url "/sendsms"
                   t.body = sms_load
                end         
    #puts resp_check.body
    #response = resp_check.body
    #response =  eval(response)
    #response = response[1..response.length - 2]
    #final = response.split(',')
    #saveMessage(name,mobile_number,refID,final[0],final[1])
end



def saveMobileMoney(resp_code,resp_desc,trans_ref,am_refid) 
  
  MmLog.create(
    resp_code: resp_code,
    resp_desc: resp_desc,
    trans_ref: trans_ref,
    am_refid: am_refid,
    created_at: Time.now
  )


end



def saveSMS(sender,recipient,msg)
  SMS.create(
    sender: sender,
    recipient: recipient,
    msg: msg
  )
end



  
